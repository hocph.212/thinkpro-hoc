import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () =>
  new Vuex.Store({
    state: {
      cart: [],
      compares: [],
    },
    getters: {
      totalPrice(state) {
        const cart = state.cart
        let total = 0
        cart.forEach((p) => {
          const money = p.price || 0
          total += money
        })
        return total
      },
      totalMoney(state) {
        const cart = state.cart
        let total = 0
        cart.forEach((p) => {
          const money = p.sale_price || p.price || 0
          total += money
        })
        return total
      },
      totalSaleMoney(state) {
        const cart = state.cart
        let total = 0
        cart.forEach((p) => {
          const money = p.sale_price ? p.price - p.sale_price : 0
          total += money
        })
        return total
      },
    },
    mutations: {
      add(state, product) {
        if (product.sale && typeof product.sale === 'object') {
          product.sale_price = product.sale.sale_price
        }
        state.cart.push(product)
      },
      remove(state, index) {
        state.cart.splice(index, 1)
      },
      addcompare(state, product) {
        state.compares.push(product)
      },
      removecompare(state, index) {
        state.compares.splice(index, 1)
      },
      removeallcompare(state, index) {
        state.compares = []
      },
    },
  })

export default store
