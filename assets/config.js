export default {
  baseImageUrl: 'https://admin.thinkpro.vn/',
  baseUrl: 'https://admin.thinkpro.vn/api/',
  resizeImage: function rewriteUrl(url, width, height) {
    const arr = url.split('/')
    let last = arr.pop()
    last = width + 'x' + height + '_' + last
    arr.push(last)
    return arr.join('/')
  },
}
