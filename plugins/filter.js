import Vue from 'vue'

Vue.filter('formatNumber', function (value) {
  if (value === 0 || value === null) return 'Chưa có giá bán'
  const formatter = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
    minimumFractionDigits: 0,
  })
  return formatter.format(value)
})

Vue.filter('truncate', function (value, limit) {
  if (!value) return ''
  if (value.length > limit) {
    value = value.substring(0, limit - 3) + '...'
  }
  return value
})

Vue.filter('textNguonhang', function (value) {
  if (value === 'Hàng nhập khẩu') {
    value = 'Nhập khẩu'
  }
  if (value === 'Hàng chính hãng') {
    value = 'Chính hãng'
  }
  return value
})
