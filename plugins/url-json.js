export default [
  {
    urlold: '/thinkpad-pl218.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/thinkpad-pl218.html?sort=3',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/thinkpad-pl218.html?sort=4',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/thinkpad-pl218.html?budget=154',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/thinkpad-pl218.html?budget=155',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/x-series-pl236.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/thinkpad-x280-pl351.html',
    urlnew: '/lenovo-thinkpad-x280',
  },
  {
    urlold:
      '/thinkpad-x280/lenovo-thinkpad-x280-i5-8250u-ram-8gb-ssd-256gb-fhd-ips-pd43951.html',
    urlnew: '/lenovo-thinkpad-x280',
  },
  {
    urlold: '/thinkpad-x390-pl455.html',
    urlnew: '/lenovo-thinkpad-x390',
  },
  {
    urlold:
      '/thinkpad-x390/lenovo-thinkpad-x390-i7-8565u-ram-8gb-ssd-256gb-fhd-ips-pd44127.html',
    urlnew: '/lenovo-thinkpad-x390',
  },
  {
    urlold:
      '/thinkpad-x390/lenovo-thinkpad-x390-i5-8265u-ram-16gb-ssd-256gb-fhd-ips-pd43808.html',
    urlnew: '/lenovo-thinkpad-x390',
  },
  {
    urlold:
      '/thinkpad-x390/lenovo-thinkpad-x390-i5-8265u-ram-8gb-ssd-256gb-fhd-ips-pd43809.html',
    urlnew: '/lenovo-thinkpad-x390',
  },
  {
    urlold: '/yoga-pl238.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/thinkpad-x1-yoga-pl366.html',
    urlnew: '/lenovo-thinkpad-x1-yoga-gen-1',
  },
  {
    urlold: '/x1-carbon-series-pl365.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/thinkpad-x1-carbon-pl370.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold: '/thinkpad-x1-carbon-gen-7-pl456.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-7/lenovo-thinkpad-x1-carbon-gen-7-i5-8265u-ram-16gb-ssd-512gb-2k-ips-pd44027.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-7/lenovo-thinkpad-x1-carbon-gen-7-i5-8265u-ram-8gb-ssd-256gb-fhd-ips-pd44006.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-7/lenovo-thinkpad-x1-carbon-gen-7-i7-10710u-ram-16gb-ssd-1tb-uhd-ips-pd44009.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-7/lenovo-thinkpad-x1-carbon-gen-7-i5-8365u-ram-16gb-ssd-256gb-fhd-ips-pd44090.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-7/lenovo-thinkpad-x1-carbon-gen-7-i7-10710u-ram-16gb-ssd-256gb-fhd-ips-pd44041.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-7/lenovo-thinkpad-x1-carbon-gen-7-i7-10510u-ram-16gb-ssd-256gb-fhd-ips-pd44016.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-7/lenovo-thinkpad-x1-carbon-gen-7-i7-10710u-ram-16gb-ssd-1tb-fhd-ips-pd44077.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-7/lenovo-thinkpad-x1-carbon-gen-7-i7-8565u-ram-8gb-ssd-256gb-fhd-ips-pd44089.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-7',
  },
  {
    urlold: '/thinkpad-x1-carbon-gen-6-pl409.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-6',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-6/lenovo-thinkpad-x1-carbon-gen-6-i5-8250u-ram-8gb-ssd-256gb-fhd-ips-pd43880.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-6',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-6/lenovo-thinkpad-x1-carbon-gen-6-i7-8550u-ram-8gb-ssd-256gb-fhd-ips-pd43932.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-6',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-6/thinkpad-x1-carbon-gen-6-i7-8650u-ram-8gb-ssd-256gb-fhd-ips-silver-pd44021.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-6',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-6/thinkpad-x1-carbon-gen-6-i7-8650u-ram-16gb-ssd-256gb-fhd-ips-silver-pd43992.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-6',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-6/lenovo-thinkpad-x1-carbon-gen-6-i7-8650u-ram-16gb-ssd-512gb-fhd-ips-pd43560.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-6',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-6/lenovo-thinkpad-x1-carbon-gen-6-i7-8550u-ram-16gb-ssd-256gb-fhd-ips-pd43932.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-6',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-6/lenovo-thinkpad-x1-carbon-gen-6-i7-8650u-ram-16gb-ssd-1tb-fhd-ips-pd43560.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-6',
  },
  {
    urlold: '/thinkpad-x1-carbon-gen-5-pl408.html',
    urlnew: '\n/lenovo-thinkpad-x1-carbon-gen-5',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-5/lenovo-thinkpad-x1-carbon-gen-5-i5-6300u-ssd-256gb-ram-8gb-fhd-ips-pd43926.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-5',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-5/lenovo-thinkpad-x1-carbon-gen-5-i5-6200u-ssd-180gb-ram-8gb-fhd-ips-pd43927.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-5',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-5/lenovo-thinkpad-x1-carbon-gen-5-i5-6200u-ssd-512gb-ram-8gb-fhd-ips-pd43925.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-5',
  },
  {
    urlold: '/thinkpad-x1-carbon-gen-8-pl537.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-8',
  },
  {
    urlold:
      '/thinkpad-x1-carbon-gen-8/lenovo-thinkpad-x1-carbon-gen-8-i7-10810u-ram-16gb-ssd-2tb-4k-ips-pd44028.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-8',
  },
  {
    urlold: '/thinkpad-x1-extreme-pl405.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/thinkpad-x1-extreme-gen-2-pl511.html',
    urlnew: '/lenovo-thinkpad-x1-extreme-gen-2',
  },
  {
    urlold:
      '/thinkpad-x1-extreme-gen-2/thinkpad-x1-extreme-gen-2-i7-9850h-ram-16gb-ssd-512gb-gtx1650-uhd-ips-pd43979.html',
    urlnew: '/lenovo-thinkpad-x1-extreme-gen-2',
  },
  {
    urlold:
      '/thinkpad-x1-extreme-gen-2/thinkpad-x1-extreme-gen-2-i7-9750h-ram-8gb-ssd-256gb-gtx1650-fhd-ips-pd44093.html',
    urlnew: '/lenovo-thinkpad-x1-extreme-gen-2',
  },
  {
    urlold:
      '/thinkpad-x1-extreme-gen-1/thinkpad-x1-extreme-i7-8750h-ram-16gb-ssd-512gb-fhd-gtx-1050ti-pd43697.html',
    urlnew: '/lenovo-thinkpad-x1-extreme-gen-1',
  },
  {
    urlold:
      '/thinkpad-x1-extreme-gen-1/thinkpad-x1-extreme-i7-8750h-ram-16gb-ssd-512gb-uhd-gtx-1050ti-pd43697.html',
    urlnew: '/lenovo-thinkpad-x1-extreme-gen-1',
  },
  {
    urlold: '/thinkpad-p1-pl480.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold:
      '/thinkpad-p1/thinkpad-p1-i5-8400h-ram-8gb-ssd-256gb-quadro-p1000-fhd-ips-pd43882.html',
    urlnew: '/lenovo-thinkpad-p1-gen-1',
  },
  {
    urlold:
      '/thinkpad-p1/thinkpad-p1-i7-8750h-ram-8gb-ssd-256gb-quadro-p1000-fhd-ips-pd43698.html',
    urlnew: '/lenovo-thinkpad-p1-gen-1',
  },
  {
    urlold:
      '/thinkpad-p1/thinkpad-p1-i5-8400h-ram-16gb-ssd-256gb-quadro-p1000-fhd-ips-pd43817.html',
    urlnew: '/lenovo-thinkpad-p1-gen-1',
  },
  {
    urlold:
      '/thinkpad-p1/thinkpad-p1-i7-8750h-ram-16gb-ssd-512gb-quadro-p1000-fhd-ips-pd43756.html',
    urlnew: '/lenovo-thinkpad-p1-gen-1',
  },
  {
    urlold:
      '/thinkpad-p1/thinkpad-p1-i5-8400h-ram-16gb-ssd-512gb-quadro-p1000-fhd-ips-pd43817.html',
    urlnew: '/lenovo-thinkpad-p1-gen-1',
  },
  {
    urlold:
      '/thinkpad-p1/thinkpad-p1-i7-8750h-ram-32gb-ssd-1tb-quadro-p1000-fhd-ips-pd44108.html',
    urlnew: '/lenovo-thinkpad-p1-gen-1',
  },
  {
    urlold:
      '/thinkpad-p1/thinkpad-p1-i7-8850h-ram-16gb-ssd-512gb-quadro-p2000-fhd-ips-pd43892.html',
    urlnew: '/lenovo-thinkpad-p1-gen-1',
  },
  {
    urlold: '/thinkpad-p1-gen-2-pl481.html',
    urlnew: '/lenovo-thinkpad-p1-gen-2',
  },
  {
    urlold: '/thinkpad-p1-gen-2-pl481.html',
    urlnew: '/lenovo-thinkpad-p1-gen-2',
  },
  {
    urlold:
      '/thinkpad-p1-gen-2/thinkpad-p1-gen-2-i7-9750h-ram-16gb-ssd-512gb-fhd-quadro-t2000-pd44046.html',
    urlnew: '/lenovo-thinkpad-p1-gen-2',
  },
  {
    urlold:
      '/thinkpad-p1-gen-2/thinkpad-p1-gen-2-i7-9750h-ram-16gb-ssd-256gb-fhd-quadro-t1000-pd44020.html',
    urlnew: '/lenovo-thinkpad-p1-gen-2',
  },
  {
    urlold:
      '/thinkpad-p50/thinkpad-p50-i7-6820hq-ram-16gb-ssd-256gb-m1000m-fhd-ips-pd43840.html',
    urlnew: '/lenovo-thinkpad-p50',
  },
  {
    urlold: '/thinkpad-p50-pl352.html',
    urlnew: '/lenovo-thinkpad-p50',
  },
  {
    urlold:
      '/thinkpad-p50/thinkpad-p50-i7-6700hq-ram-16gb-ssd-512gb-m1000m-fhd-ips-pd43968.html',
    urlnew: '/lenovo-thinkpad-p50',
  },
  {
    urlold:
      '/thinkpad-p50/thinkpad-p50-i7-6700hq-ram-8gb-ssd-256gb-m1000m-fhd-ips-pd43967.html',
    urlnew: '/lenovo-thinkpad-p50',
  },
  {
    urlold:
      '/thinkpad-p50/thinkpad-p50-xeon-e3-1505v5-ram-16gb-ssd-512gb-quadro-m2000m-fhd-ips-pd43810.html',
    urlnew: '/lenovo-thinkpad-p50',
  },
  {
    urlold:
      '/thinkpad-p50/thinkpad-p50-i7-6700hq-ram-16gb-ssd-256gb-m1000m-fhd-ips-pd43885.html',
    urlnew: '/lenovo-thinkpad-p50',
  },
  {
    urlold:
      '/thinkpad-p50/thinkpad-p50-i7-6820hq-ram-16gb-ssd-256gb-m1000m-fhd-ips-pd43840.html',
    urlnew: '/lenovo-thinkpad-p50',
  },
  {
    urlold: '/thinkpad-p51-pl353.html',
    urlnew: '/lenovo-thinkpad-p51',
  },
  {
    urlold:
      '/thinkpad-p51/thinkpad-p51-i7-6820hq-ram-16gb-ssd-512gb-m2200m-fhd-ips-pd44095.html',
    urlnew: '/lenovo-thinkpad-p51',
  },
  {
    urlold:
      '/thinkpad-p51/thinkpad-p51-i7-6820hq-ram-8gb-hdd-500gb-m1200m-fhd-ips-pd43468.html',
    urlnew: '/lenovo-thinkpad-p51',
  },
  {
    urlold:
      '/thinkpad-p51/thinkpad-p51-i7-7700hq-ssd-512gb-ram-16gb-4k-uhd-ips-m1200m-pd43956.html',
    urlnew: '/lenovo-thinkpad-p51',
  },
  {
    urlold: '/thinkpad-p53-pl482.html',
    urlnew: '/lenovo-thinkpad-p53',
  },
  {
    urlold: '/thinkpad-p53s-pl483.html',
    urlnew: '/lenovo-thinkpad-p53',
  },
  {
    urlold:
      '/thinkpad-p53/thinkpad-p53-i9-9980h-ram-32gb-ssd-1tb-15-6-fhd-ips-nvidia-quadro-rtx-4000-pd44065.html',
    urlnew: '/lenovo-thinkpad-p53',
  },
  {
    urlold:
      '/thinkpad-p53/thinkpad-p53-i7-9750h-ram-16gb-ssd-256gb-fhd-ips-t2000-pd43855.html',
    urlnew: '/lenovo-thinkpad-p53',
  },
  {
    urlold:
      '/thinkpad-p53/thinkpad-p53-i5-9400h-ram-8gb-ssd-256gb-fhd-ips-t1000-pd44058.html',
    urlnew: '/lenovo-thinkpad-p53',
  },
  {
    urlold:
      '/thinkpad-p53s/thinkpad-p53s-i7-8665u-ram-16gb-ssd-512gb-fhd-p520-pd43853.html',
    urlnew: '/lenovo-thinkpad-p53s',
  },
  {
    urlold: '/thinkpad-p52-pl439.html',
    urlnew: '/lenovo-thinkpad-p52',
  },
  {
    urlold: '/thinkpad-p52s-pl445.html',
    urlnew: '/lenovo-thinkpad-p52s',
  },
  {
    urlold:
      '/thinkpad-p52s/lenovo-thinkpad-p52s-i7-8550u-ram-16gb-ssd-512gb-quadro-p500-fhd-pd43763.html',
    urlnew: '/lenovo-thinkpad-p52s',
  },
  {
    urlold: '/thinkpad-p43s-pl484.html',
    urlnew: '/lenovo-thinkpad-p43s',
  },
  {
    urlold:
      '/thinkpad-p43s/thinkpad-p43s-i7-8665u-ram-16gb-ssd-512gb-fhd-p520-pd43854.html',
    urlnew: '/lenovo-thinkpad-p43s',
  },
  {
    urlold: '/thinkpad-t450s-pl546.html',
    urlnew: '/lenovo-thinkpad-t450s',
  },
  {
    urlold:
      '/thinkpad-t450s/thinkpad-t450s-i7-5600u-ram-8gb-hdd-500gb-fhd-ips-pd44072.html',
    urlnew: '/lenovo-thinkpad-t450s',
  },
  {
    urlold:
      '/thinkpad-t450s/thinkpad-t450s-i5-5300u-ram-8gb-hdd-500gb-fhd-ips-pd44071.html',
    urlnew: '/lenovo-thinkpad-t450s',
  },
  {
    urlold:
      '/thinkpad-t450s/thinkpad-t450s-i5-5300u-ram-8gb-hdd-500gb-hd-pd44119.html',
    urlnew: '/lenovo-thinkpad-t450s',
  },
  {
    urlold: '/thinkpad-t460s-pl454.html',
    urlnew: '/lenovo-thinkpad-t460s',
  },
  {
    urlold:
      '/thinkpad-t460s/thinkpad-t460s-i5-6300u-ram-8gb-ssd-256gb-fhd-ips-pd43850.html',
    urlnew: '/lenovo-thinkpad-t460s',
  },
  {
    urlold:
      '/thinkpad-t460s/thinkpad-t460-i5-6300u-ram-8gb-ssd-128gb-fhd-ips-pd44073.html',
    urlnew: '/lenovo-thinkpad-t460s',
  },
  {
    urlold:
      '/thinkpad-t460s/thinkpad-t460s-i7-6600u-ram-8gb-ssd-256gb-fhd-ips-pd44066.html',
    urlnew: '/lenovo-thinkpad-t460s',
  },
  {
    urlold:
      '/thinkpad-t460s/thinkpad-t460-i5-6300u-ram-16gb-ssd-512gb-fhd-touch-pd43859.html',
    urlnew: '/lenovo-thinkpad-t460s',
  },
  {
    urlold:
      '/thinkpad-t460s/thinkpad-t460-i5-6300u-ram-16gb-ssd-240gb-fhd-touch-pd44030.html',
    urlnew: '/lenovo-thinkpad-t460s',
  },
  {
    urlold:
      '/thinkpad-t470s/thinkpad-t470s-i7-6600u-ram-20gb-ssd-256gb-fhd-ips-pd43965.html',
    urlnew: '/lenovo-thinkpad-t470s',
  },
  {
    urlold:
      '/thinkpad-t470s/thinkpad-t470s-i5-6300u-ram-12gb-ssd-256gb-fhd-ips-pd44161.html',
    urlnew: '/lenovo-thinkpad-t470s',
  },
  {
    urlold:
      '/thinkpad-t470s/thinkpad-t470s-i5-6300u-ram-8gb-ssd-256gb-fhd-ips-pd44160.html',
    urlnew: '/lenovo-thinkpad-t470s',
  },
  {
    urlold: '/thinkpad-t580-pl479.html',
    urlnew: '/lenovo-thinkpad-t580',
  },
  {
    urlold:
      '/thinkpad-t580/lenovo-thinkpad-t580-i7-8550u-ram-8gb-ssd-256gb-fhd-ips-pd43846.html',
    urlnew: '/lenovo-thinkpad-t580',
  },
  {
    urlold:
      '/thinkpad-t480/lenovo-thinkpad-t480-i5-8350u-ram-8gb-ssd-256gb-fhd-ips-touch-pd43930.html',
    urlnew: '/lenovo-thinkpad-t480s',
  },
  {
    urlold: '/thinkpad-t480s-pl303.html',
    urlnew: '/lenovo-thinkpad-t480s',
  },
  {
    urlold: '/thinkpad-t480-pl318.html',
    urlnew: '/lenovo-thinkpad-t480',
  },
  {
    urlold:
      '/thinkpad-t480s/lenovo-thinkpad-t480s-i7-8650u-ram-8gb-ssd-256gb-fhd-ips-touch-pd43555.html',
    urlnew: '/lenovo-thinkpad-t480s',
  },
  {
    urlold:
      '/thinkpad-t480s/lenovo-thinkpad-t480s-i7-8650u-ram-8gb-ssd-256gb-fhd-ips-pd43553.html',
    urlnew: '/lenovo-thinkpad-t480s',
  },
  {
    urlold:
      '/thinkpad-t480/lenovo-thinkpad-t480-i5-8350u-ram-8gb-ssd-512gb-fhd-ips-pd43929.html',
    urlnew: '/lenovo-thinkpad-t480',
  },
  {
    urlold:
      '/thinkpad-t480s/lenovo-thinkpad-t480s-i5-8350u-ram-8gb-ssd-256gb-fhd-ips-pd43964.html',
    urlnew: '/lenovo-thinkpad-t480s',
  },
  {
    urlold:
      '/thinkpad-t480/lenovo-thinkpad-t480-i7-8650u-ram-32gb-ssd-1tb-fhd-ips-pd43551.html',
    urlnew: '/lenovo-thinkpad-t480',
  },
  {
    urlold:
      '/thinkpad-t480/lenovo-thinkpad-t480-i3-8130u-ram-8gb-ssd-256gb-hd-pd43902.html',
    urlnew: '/lenovo-thinkpad-t480',
  },
  {
    urlold: '/thinkpad-t570-pl397.html',
    urlnew: '/lenovo-thinkpad-t570',
  },
  {
    urlold:
      '/thinkpad-t570/lenovo-thinkpad-t570-i5-7300u-ram-8gb-ssd-128gb-fhd-ips-pd43881.html',
    urlnew: '/lenovo-thinkpad-t570',
  },
  {
    urlold: '/thinkpad-t590-pl547.html',
    urlnew: '/lenovo-thinkpad-t590',
  },
  {
    urlold:
      '/thinkpad-t590/lenovo-thinkpad-t590-i5-8265u-ram-16gb-ssd-512gb-fhd-ips-pd44079.html',
    urlnew: '/lenovo-thinkpad-t590',
  },
  {
    urlold:
      '/thinkpad-t590/lenovo-thinkpad-t590-i5-8265u-ram-8gb-ssd-256gb-fhd-ips-pd44076.html',
    urlnew: '/lenovo-thinkpad-t590',
  },
  {
    urlold:
      '/thinkpad-t590/lenovo-thinkpad-t590-i5-8265u-ram-16gb-ssd-256gb-fhd-ips-pd44078.html',
    urlnew: '/lenovo-thinkpad-t590',
  },
  {
    urlold: '/thinkpad-t490s-pl444.html',
    urlnew: '/lenovo-thinkpad-t490s',
  },
  {
    urlold: '/thinkpad-t490-pl443.html',
    urlnew: '/lenovo-thinkpad-t490',
  },
  {
    urlold:
      '/thinkpad-t490s/lenovo-thinkpad-t490s-i5-8265u-ram-8gb-ssd-256gb-14-fhd-ips-pd43761.html',
    urlnew: '/lenovo-thinkpad-t490s',
  },
  {
    urlold:
      '/thinkpad-t490/lenovo-thinkpad-t490-core-i7-8565u-ram-8gb-ssd-512gb-14-rsquo-rsquo-fhd-ips-mx250-pd44033.html',
    urlnew: '/lenovo-thinkpad-t490',
  },
  {
    urlold: '/thinkpad-a485-pl503.html',
    urlnew: '/lenovo-thinkpad-a485',
  },
  {
    urlold:
      '/thinkpad-a485/lenovo-thinkpad-a485-ryzen-3-2300u-hdd-500gb-ram-4gb-hd-vega-6-pd43958.html',
    urlnew: '/lenovo-thinkpad-a485',
  },
  {
    urlold:
      '/thinkpad-a485/lenovo-thinkpad-a485-ryzen-7-2700u-ssd-256gb-ram-8gb-fhd-ips-vega-10-pd43957.html',
    urlnew: '/lenovo-thinkpad-a485',
  },
  {
    urlold:
      '/thinkpad-w540/thinkpad-w540-i7-4800mq-ram-8gb-ssd-256gb-quadro-k1100m-fhd-pd44049.html',
    urlnew: '/lenovo-thinkpad-w540',
  },
  {
    urlold:
      '/thinkpad-w540/thinkpad-w540-i7-4800mq-ram-16gb-ssd-256gb-quadro-k1100m-wqhd-pd44048.html',
    urlnew: '/lenovo-thinkpad-w540',
  },
  {
    urlold: '/thinkpad-w541-pl543.html',
    urlnew: '/lenovo-thinkpad-w541',
  },
  {
    urlold:
      '/thinkpad-w541/thinkpad-w541-i7-4910mq-ram-8gb-ssd-256gb-1tb-fhd-k1100m-pd44052.html',
    urlnew: '/lenovo-thinkpad-w541',
  },
  {
    urlold:
      '/thinkpad-w541/thinkpad-w541-i7-4810mq-ram-16gb-ssd-512gb-wqhd-k1100m-pd44050.html',
    urlnew: '/lenovo-thinkpad-w541',
  },
  {
    urlold:
      '/thinkpad-w541/thinkpad-w541-i7-4810mq-ram-8gb-ssd-256gb-fhd-k1100m-pd44051.html',
    urlnew: '/lenovo-thinkpad-w541',
  },
  {
    urlold: '/lenovo-pl225.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/lenovo-ideapad-720s-pl467.html',
    urlnew: '/lenovo-ideapad-720s',
  },
  {
    urlold:
      '/lenovo-ideapad-720s/lenovo-ideapad-720s-i5-8250u-ssd-256gb-ram-8gb-fhd-ips-pd43828.html',
    urlnew: '/lenovo-ideapad-720s',
  },
  {
    urlold:
      '/lenovo-ideapad-720s/lenovo-ideapad-720s-i5-8250u-ssd-256gb-ram-8gb-fhd-ips-mx150-pd43842.html',
    urlnew: '/lenovo-ideapad-720s',
  },
  {
    urlold:
      '/lenovo-ideapad-720s/lenovo-ideapad-720s-i7-8550u-ssd-512gb-ram-8gb-fhd-ips-pd43857.html',
    urlnew: '/lenovo-ideapad-720s',
  },
  {
    urlold: '/lenovo-ideapad-l340-pl488.html',
    urlnew: '/lenovo-ideapad-L340',
  },
  {
    urlold:
      '/lenovo-ideapad-l340/lenovo-ideapad-l340-i7-9750h-ram-8gb-hdd-1tb-gtx-1050-fhd-ips-pd43874.html',
    urlnew: '/lenovo-ideapad-L340',
  },
  {
    urlold:
      '/lenovo-ideapad-l340/lenovo-ideapad-l340-i5-9300h-ram-8gb-ssd-512-gtx-1050-fhd-ips-pd43869.html',
    urlnew: '/lenovo-ideapad-L340',
  },
  {
    urlold:
      '/lenovo-ideapad-l340/lenovo-ideapad-l340-i5-9300hf-ram-8gb-ssd-256-gtx-1050-fhd-ips-pd44133.html',
    urlnew: '/lenovo-ideapad-L340',
  },
  {
    urlold:
      '/lenovo-ideapad-l340/lenovo-ideapad-l340-i5-9300h-ram-8gb-ssd-256-gtx-1050-fhd-ips-pd44133.html',
    urlnew: '/lenovo-ideapad-L340',
  },
  {
    urlold:
      '/lenovo-ideapad-l340/lenovo-ideapad-l340-i5-9300hf-ram-8gb-ssd-512-gtx-1050-fhd-ips-pd43869.html',
    urlnew: '/lenovo-ideapad-L340',
  },
  {
    urlold:
      '/ideapad/lenovo-ideapad-s340-15iil-i7-1065g7-ram-8gb-ssd-256gb-fhd-ips-pd44047.html',
    urlnew: '/lenovo-ideapad-s340-15',
  },
  {
    urlold: '/lenovo-ideapad-730s-pl489.html',
    urlnew: '/lenovo-ideapad-730s',
  },
  {
    urlold:
      '/lenovo-ideapad-730s/lenovo-ideapad-730s-i7-8565u-ssd-256gb-ram-8gb-fhd-ips-pd43888.html',
    urlnew: '/lenovo-ideapad-730s',
  },
  {
    urlold:
      '/lenovo-ideapad-730s/lenovo-ideapad-730s-i7-8565u-ssd-512gb-ram-16gb-fhd-ips-pd43921.html',
    urlnew: '/lenovo-ideapad-730s',
  },
  {
    urlold:
      '/lenovo-ideapad-730s/lenovo-ideapad-730s-i5-8265u-256gb-ssd-8gb-ram-fhd-ips-pd43897.html',
    urlnew: '/lenovo-ideapad-730s',
  },
  {
    urlold: '/lenovo-ideapad-s540-pl555.html',
    urlnew: '/lenovo-ideapad-s540-15',
  },
  {
    urlold:
      '/lenovo-ideapad-s540/lenovo-ideapad-s540-15iwl-core-i5-8265u-ram-12gb-ssd-256gb-15-6-fhd-ips-pd44099.html',
    urlnew: '/lenovo-ideapad-s540-15',
  },
  {
    urlold:
      '/lenovo-ideapad-s540/lenovo-ideapad-s540-15iwl-core-i5-8265u-ram-12gb-ssd-256gb-15-6-fhd-ips-pd44137.html',
    urlnew: '/lenovo-ideapad-s540-15',
  },
  {
    urlold: '/thinkbook-pl538.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/lenovo-thinkbook-13s-pl539.html',
    urlnew: '/lenovo-thinkbook-13s',
  },
  {
    urlold:
      '/lenovo-thinkbook-13s/lenovo-thinkbook-13s-core-i5-8265u-ram-8gb-256gb-ssd-fhd-ips-pd44039.html',
    urlnew: '/lenovo-thinkbook-13s',
  },
  {
    urlold:
      '/lenovo-thinkbook-14s/lenovo-thinkbook-14s-core-i5-8265u-ram-8gb-256gb-ssd-amd-radeon-540x-fhd-ips-pd44037.html',
    urlnew: '/lenovo-thinkbook-14s',
  },
  {
    urlold:
      '/lenovo-thinkbook-14s/lenovo-thinkbook-14s-core-i7-8565u-ram-8gb-256gb-ssd-amd-radeon-540x-fhd-ips-pd44038.html',
    urlnew: '/lenovo-thinkbook-14s',
  },
  {
    urlold: '/lenovo-legion-y545-pl492.html',
    urlnew: '/lenovo-legion-y545',
  },
  {
    urlold:
      '/lenovo-legion-y545/lenovo-legion-y545-i7-9750h-ram-16gb-ssd-512gb-hdd-1tb-fhd-gtx-1660ti-pd44023.html',
    urlnew: '/lenovo-legion-y545',
  },
  {
    urlold:
      '/lenovo-legion-y545/lenovo-legion-y545-i7-9750h-ram-16gb-ssd-512gb-hdd-1tb-fhd-gtx-1660ti-pd44018.html',
    urlnew: '/lenovo-legion-y545',
  },
  {
    urlold:
      '/lenovo-legion-y545/lenovo-legion-y545-i7-9750h-ram-16gb-ssd-256gb-hdd-1tb-fhd-gtx-1650-pd43895.html',
    urlnew: '/lenovo-legion-y545',
  },
  {
    urlold:
      '/lenovo-legion-y545/lenovo-legion-y545-i7-9750h-ssd-512gb-ram-16gb-fhd-144hz-rtx-2060-pd43937.html',
    urlnew: '/lenovo-legion-y545',
  },
  {
    urlold: '/lenovo-legion-y530-pl369.html',
    urlnew: '/lenovo-legion-y530',
  },
  {
    urlold:
      '/lenovo-legion-y530/lenovo-legion-y530-i5-8300h-ram-8gb-ssd-128gb-hdd-1tb-gtx-1050-fhd-pd43678.html',
    urlnew: '/lenovo-legion-y530',
  },
  {
    urlold: '/lenovo-legion-y740-pl452.html',
    urlnew: '/lenovo-legion-y740',
  },
  {
    urlold:
      '/lenovo-legion-y740/lenovo-legion-y740-i7-8750h-ram-16gb-ssd-256gb-fhd-ips-144hz-rtx-2060-pd43798.html',
    urlnew: '/lenovo-legion-y740',
  },
  {
    urlold:
      '/lenovo-legion-y7000/lenovo-legion-y7000-i5-8300h-ram-8gb-ssd-128gb-hdd-1tb-fhd-1050ti-pd43843.html',
    urlnew: '/lenovo-legion-y7000',
  },
  {
    urlold:
      '/lenovo-legion-y7000/lenovo-legion-y7000-i5-8300h-ram-16gb-ssd-256gb-fhd-ips-gtx1050ti-pd43772.html',
    urlnew: '/lenovo-legion-y7000',
  },
  {
    urlold:
      '/lenovo-legion-c730/lenovo-legion-c730-i9-9900k-1tb-512gb-ssd-32gb-bt-win10-rtx-2080-pd44012.html',
    urlnew: '/lenovo-legion-c730',
  },
  {
    urlold:
      '/lenovo-legion-c730/lenovo-legion-c730-i9-9900k-512gb-ssd-16gb-bt-win10-rtx-2080-pd44060.html',
    urlnew: '/lenovo-legion-c730',
  },
  {
    urlold:
      '/lenovo-ideacentre-730s-all-in-one/lenovo-ideacentre-730s-aio-i7-8550u-hdd-1tb-ram-8gb-23-8-fhd-ips-touch-pd43939.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: 'DELL',
  },
  {
    urlold: '/dell-pl224.html?sort=4',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-pl224.html?budget=155',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-pl224.html?budget=154',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-pl224.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/xps-pl231.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/xps-pl231.html?sort=4',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-xps-15-pl297.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-xps-15-9500-pl575.html',
    urlnew: '/dell-xps-15-9500',
  },
  {
    urlold:
      '/dell-xps-15-9550/dell-xps-15-9550-i7-6700hq-ram-8gb-ssd-256gb-fhd-gtx960m-pd43395.html',
    urlnew: '/dell-xps-15-9550',
  },
  {
    urlold:
      '/dell-xps-15-9550/dell-xps-15-9550-i7-6700hq-ram-32gb-ssd-1tb-4k-uhd-touch-960m-pd43285.html',
    urlnew: '/dell-xps-15-9550',
  },
  {
    urlold: '/dell-xps-15-9570-pl298.html',
    urlnew: '/dell-xps-15-9570',
  },
  {
    urlold:
      '/dell-xps-15-9570/dell-xps-15-9570-i5-8300h-ram-8gb-ssd-256gb-uhd-touch-gtx1050-pd43861.html',
    urlnew: '/dell-xps-15-9570',
  },
  {
    urlold:
      '/dell-xps-15-9570/dell-xps-15-9570-i7-8750h-ram-16gb-ssd-512gb-fhd-ips-nvidia-gtx1050ti-pd43593.html',
    urlnew: '/dell-xps-15-9570',
  },
  {
    urlold:
      '/dell-xps-15-9570/dell-xps-15-9570-i7-8750h-ssd-1tb-ram-32gb-uhd-touch-gtx1050ti-pd43575.html',
    urlnew: '/dell-xps-15-9570',
  },
  {
    urlold:
      '/dell-xps-15-9570/dell-xps-15-9570-i5-8300h-ram-8gb-ssd-256gb-fhd-ips-gtx1050-pd43883.html',
    urlnew: '/dell-xps-15-9570',
  },
  {
    urlold:
      '/dell-xps-15-9570-pl298.html?fbclid=IwAR1bbgrwZ4s47429IjhPRLAvBNVnNHERA1g5_g-PtzETfAEkXNwTej7sXD4',
    urlnew: '/dell-xps-15-9570',
  },
  {
    urlold: '/dell-xps-15-9575-pl317.html',
    urlnew: '/dell-xps-15-9575',
  },
  {
    urlold:
      '/dell-xps-15-9575/dell-xps-15-9575-i7-8705g-ram-16gb-ssd-512gb-4k-uhd-pd43543.html',
    urlnew: '/dell-xps-15-9575',
  },
  {
    urlold: '/dell-xps-15-9560-pl259.html',
    urlnew: '/dell-xps-15-9560',
  },
  {
    urlold:
      '/dell-xps-15-9560/dell-xps-15-9560-i5-7300hq-ram-8gb-ssd-256gb-fhd-gtx1050-pd43691.html',
    urlnew: '/dell-xps-15-9560',
  },
  {
    urlold:
      '/dell-xps-15-9560/dell-xps-15-9560-i7-7700hq-ram-16gb-ssd-512gb-fhd-gtx1050-pd43492.html',
    urlnew: '/dell-xps-15-9560',
  },
  {
    urlold:
      '/dell-xps-15-9560/dell-xps-15-9560-i7-7700hq-ram-16gb-ssd-512gb-15-6-uhd-gtx-1050-pd43517.html',
    urlnew: '/dell-xps-15-9560',
  },
  {
    urlold: '/dell-xps-15-7590-pl485.html',
    urlnew: '/dell-xps-15-7590',
  },
  {
    urlold:
      '/dell-xps-15-7590/dell-xps-15-7590-i9-9980hk-ram-32gb-ssd-2tb-15-6-oled-gtx-1650-4gb-pd43993.html',
    urlnew: '/dell-xps-15-7590',
  },
  {
    urlold:
      '/dell-xps-15-7590/dell-xps-15-7590-i5-9300h-ram-8gb-ssd-256gb-fhd-ips-pd43938.html',
    urlnew: '/dell-xps-15-7590',
  },
  {
    urlold:
      '/dell-xps-15-7590/dell-xps-15-7590-i7-9750h-ram-16gb-ssd-512gb-gtx-1650-fhd-ips-pd43862.html',
    urlnew: '/dell-xps-15-7590',
  },
  {
    urlold:
      '/dell-xps-15-7590/dell-xps-15-7590-i7-9750h-ram-16gb-ssd-256gb-fhd-ips-gtx-1650-pd44129.html',
    urlnew: '/dell-xps-15-7590',
  },
  {
    urlold:
      '/dell-xps-15-7590/dell-xps-15-7590-i7-9750h-ram-8gb-ssd-256gb-gtx-1650-fhd-ips-pd43944.html',
    urlnew: '/dell-xps-15-7590',
  },
  {
    urlold: '/dell-xps-15-9550-pl254.html',
    urlnew: '/dell-xps-15-9550',
  },
  {
    urlold:
      '/dell-xps-15-9550/dell-xps-15-9550-i7-6700hq-ram-16gb-ssd-512gb-4k-uhd-touch-960m-pd43417.html',
    urlnew: '/dell-xps-15-9550',
  },
  {
    urlold: '/dell-xps-13-pl292.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-xps-13-9300-pl545.html',
    urlnew: '/dell-xps-13-9360',
  },
  {
    urlold: '/dell-xps-13-7390-pl531.html',
    urlnew: '/dell-xps-13-7390',
  },
  {
    urlold: '/dell-xps-13-9380-pl446.html',
    urlnew: '/dell-xps-13-9380',
  },
  {
    urlold:
      '/dell-xps-13-9380/dell-xps-13-9380-core-i7-8565u-ram-8gb-ssd-256gb-fhd-ips-pd43972.html',
    urlnew: '/dell-xps-13-9380',
  },
  {
    urlold:
      '/dell-xps-13-9380/dell-xps-13-9380-core-i5-8265u-ram-8gb-ssd-256gb-fhd-ips-touch-pd43765.html',
    urlnew: '/dell-xps-13-9380',
  },
  {
    urlold:
      '/dell-xps-13-9380/dell-xps-13-9380-core-i7-8565u-ram-8gb-ssd-256gb-fhd-ips-pd43766.html',
    urlnew: '/dell-xps-13-9380',
  },
  {
    urlold:
      '/dell-xps-13-9360/dell-xps-13-9360-i5-8250u-ram-8gb-ssd-256gb-fhd-ips-pd43288.html',
    urlnew: '/dell-xps-13-9360',
  },
  {
    urlold:
      '/dell-xps-13-9360/dell-xps-13-9360-i5-7200u-ram-8gb-ssd-256gb-fhd-ips-pd43288.html',
    urlnew: '/dell-xps-13-9360',
  },
  {
    urlold:
      '/dell-xps-13-9360/dell-xps-13-9360-i5-7200u-ram-8gb-ssd-256gb-fhd-ips-pd43288.html',
    urlnew: '/dell-xps-13-9360',
  },
  {
    urlold:
      '/dell-xps-13-9360/dell-xps-13-9360-i5-8250u-ram-8gb-ssd-256gb-fhd-ips-pd43873.html',
    urlnew: '/dell-xps-13-9360',
  },
  {
    urlold: '/dell-xps-13-9360-pl258.html',
    urlnew: '/dell-xps-13-9360',
  },
  {
    urlold:
      '/dell-xps-13-9300/dell-xps-13-9300-core-i5-1035g1-ram-8gb-ssd-256gb-fhd-pd44068.html',
    urlnew: '/dell-xps-13-9300',
  },
  {
    urlold:
      '/dell-xps-13-9300/dell-xps-13-9300-core-i7-1065g7-ram-16gb-ssd-512gb-uhd-pd44070.html',
    urlnew: '/dell-xps-13-9300',
  },
  {
    urlold:
      '/dell-xps-13-9300/dell-xps-13-9300-core-i7-1065g7-ram-8gb-ssd-256gb-fhd-pd44069.html',
    urlnew: '/dell-xps-13-9300',
  },
  {
    urlold: '/dell-xps-13-7390-2-in-1-pl558.html',
    urlnew: '/dell-xps-13-7390-2-in-1',
  },
  {
    urlold:
      '/dell-xps-13-7390-2-in-1/dell-xps-13-7390-2-in-1-i7-1065g7-ram-16gb-ssd-256gb-fhd-touch-pd44146.html',
    urlnew: '/dell-xps-13-7390-2-in-1',
  },
  {
    urlold:
      '/dell-xps-13-7390/dell-xps-13-7390-core-i5-10210u-ram-8gb-ssd-256gb-fhd-ips-pd43997.html',
    urlnew: '/dell-xps-13-7390',
  },
  {
    urlold:
      '/dell-xps-13-7390/dell-xps-13-7390-core-i7-10710u-ram-8gb-ssd-256gb-fhd-ips-pd44130.html',
    urlnew: '/dell-xps-13-7390',
  },
  {
    urlold:
      '/dell-xps-13-7390/dell-xps-13-7390-core-i5-10210u-ram-8gb-ssd-256gb-fhd-ips-touchscreen-pd44017.html',
    urlnew: '/dell-xps-13-7390',
  },
  {
    urlold:
      '/dell-xps-13-7390-2-in-1/dell-xps-13-7390-2-in-1-i7-1065g7-ram-16gb-ssd-512gb-fhd-touch-pd44145.html',
    urlnew: '/dell-xps-13-7390',
  },
  {
    urlold:
      '/dell-xps-13-7390-2-in-1/dell-xps-13-7390-2-in-1-i7-1065g7-ram-16gb-ssd-512gb-fhd-touch-pd44101.html',
    urlnew: '/dell-xps-13-7390',
  },
  {
    urlold:
      '/dell-xps-13-9370/dell-xps-13-9370-i7-8550u-ram-8gb-ssd-256gb-4k-touch-pd43900.html',
    urlnew: '/dell-xps-13-9370',
  },
  {
    urlold:
      '/dell-xps-13-9370/dell-xps-13-9370-i7-8550u-ram-8gb-ssd-256gb-fhd-touch-pd43966.html',
    urlnew: '/dell-xps-13-9370',
  },
  {
    urlold: '/precision-pl230.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-precision-3510-pl530.html',
    urlnew: '/dell-precision-3510',
  },
  {
    urlold:
      '/dell-precision-3510/dell-precision-3510-i7-6820hq-ram-8gb-ssd-256gb-amd-firepro-w5130m-fhd-ips-pd43995.html',
    urlnew: '/dell-precision-3510',
  },
  {
    urlold:
      '/dell-precision-3510/dell-precision-3510-i7-6820hq-ram-16gb-ssd-512gb-amd-firepro-w5130m-fhd-ips-pd43996.html',
    urlnew: '/dell-precision-3510',
  },
  {
    urlold: '/dell-precision-5530-pl333.html',
    urlnew: '/dell-precision-5530',
  },
  {
    urlold:
      '/dell-precision-5530/dell-precision-5530-i5-8400h-ram-16gb-ssd-512gb-fhd-ips-p1000-pd43903.html',
    urlnew: '/dell-precision-5530',
  },
  {
    urlold:
      '/dell-precision-5530/dell-precision-5530-i7-8850h-ram-32gb-ssd-512gb-fhd-ips-p2000-pd43980.html',
    urlnew: '/dell-precision-5530',
  },
  {
    urlold:
      '/dell-precision-5530/dell-precision-5530-i5-8300h-ram-8gb-ssd-256gb-fhd-ips-pd43903.html',
    urlnew: '/dell-precision-5530',
  },
  {
    urlold:
      '/dell-precision-5530/dell-precision-5530-e-2176m-ram-32gb-ssd-512gb-fhd-ips-p1000-pd43981.html',
    urlnew: '/dell-precision-5530',
  },
  {
    urlold:
      '/dell-precision-5530/dell-precision-5530-i7-8850h-ram-8gb-hdd-500gb-fhd-ips-vga-p1000-pd44115.html',
    urlnew: '/dell-precision-5530',
  },
  {
    urlold:
      '/dell-precision-5530/dell-precision-5530-i5-8400h-ram-32gb-ssd-1tb-uhd-touch-p1000-pd44121.html',
    urlnew: '/dell-precision-5530',
  },
  {
    urlold: '/dell-precision-5510-pl331.html',
    urlnew: '/dell-precision-5510',
  },
  {
    urlold:
      '/dell-precision-5510/dell-precision-5510-e3-1505m-v5-ram-8gb-ssd-256gb-4k-touch-m1000m-pd43977.html',
    urlnew: '/dell-precision-5510',
  },
  {
    urlold:
      '/dell-precision-5510/dell-precision-5510-e3-1505m-v5-ram-16gb-ssd-512gb-4k-touch-m1000m-pd43976.html',
    urlnew: '/dell-precision-5510',
  },
  {
    urlold:
      '/dell-precision-5510/dell-precision-5510-i7-6820hq-ram-16gb-ssd-512gb-fhd-ips-m1000m-pd43941.html',
    urlnew: '/dell-precision-5510',
  },
  {
    urlold:
      '/dell-precision-5510/dell-precision-5510-i7-6820hq-ram-8gb-ssd-256gb-4k-touch-m1000m-pd43410.html',
    urlnew: '/dell-precision-5510',
  },
  {
    urlold:
      '/dell-precision-5510/dell-precision-5510-core-i7-6820hq-ram-32gb-ssd-1tb-4k-pd43407.html',
    urlnew: '/dell-precision-5510',
  },
  {
    urlold: '/dell-precision-7000-series-pl300.html',
    urlnew: '/dell-precision-7000',
  },
  {
    urlold: '/dell-precision-5000-series-pl330.html',
    urlnew: '/dell-precision-5000',
  },
  {
    urlold:
      '/dell-precision-5520/dell-precision-5520-i7-7820hq-ram-16gb-ssd-512gb-15-6-fhd-pd43444.html',
    urlnew: '/dell-precision-5520',
  },
  {
    urlold:
      '/dell-precision-7510/dell-precision-7510-xeon-e3-1535-v5-ram-16gb-ssd-512gb-fhd-ips-m2000m-pd43352.html',
    urlnew: '/dell-precision-7510',
  },
  {
    urlold:
      '/dell-precision-7510/dell-precision-7510-i7-6820hq-ram-8gb-ssd-256gb-m1000m-fhd-ips-pd43455.html',
    urlnew: '/dell-precision-7510',
  },
  {
    urlold:
      '/dell-precision-7510/dell-precision-7510-i7-6820hq-ram-16gb-ssd-512gb-fhd-ips-m1000m-pd43908.html',
    urlnew: '/dell-precision-7510',
  },
  {
    urlold:
      '/dell-precision-7510/dell-precision-7510-xeon-e3-1535-v5-ram-16gb-ssd-512gb-fhd-ips-m2000m-pd43352.html',
    urlnew: '/dell-precision-7510',
  },
  {
    urlold:
      '/dell-precision-7510/dell-precision-7510-i7-6820hq-ram-8gb-ssd-256gb-m1000m-fhd-ips-pd43455.html',
    urlnew: '/dell-precision-7510',
  },
  {
    urlold:
      '/dell-precision-7510/dell-precision-7510-i7-6820hq-ram-16gb-ssd-256gb-fhd-ips-m1000m-pd43908.html',
    urlnew: '/dell-precision-7510',
  },
  {
    urlold: '/dell-precision-7540-pl512.html',
    urlnew: '/dell-precision-7540',
  },
  {
    urlold:
      '/dell-precision-7540/dell-precision-7540-i9-9980h-ram-32gb-ssd-512gb-fhd-ips-t2000-pd44036.html',
    urlnew: '/dell-precision-7540',
  },
  {
    urlold:
      '/dell-precision-7540/dell-precision-7540-i7-9850h-ram-32gb-ssd-512gb-fhd-ips-t1000-pd43983.html',
    urlnew: '/dell-precision-7540',
  },
  {
    urlold:
      '/dell-precision-7520/dell-precision-7520-i7-7820hq-ram-8gb-ssd-256gb-fhd-quadro-m2200m-4gb-pd43687.html',
    urlnew: '/dell-precision-7520',
  },
  {
    urlold: '/dell-precision-7530-pl336.html',
    urlnew: '/dell-precision-7530',
  },
  {
    urlold:
      '/dell-precision-7530/dell-precision-7530-i9-8950hk-ram-64gb-ssd-1tb-fhd-touch-quadro-p3200-pd43594.html',
    urlnew: '/dell-precision-7530',
  },
  {
    urlold:
      '/dell-precision-5540/dell-precision-5540-i5-9400h-ram-8gb-ssd-256gb-fhd-ips-pd44091.html',
    urlnew: '/dell-precision-5540',
  },
  {
    urlold: '/dell-precision-m4800-pl536.html',
    urlnew: '/dell-precision-m4800',
  },
  {
    urlold:
      '/dell-precision-m4800/dell-precision-m4800-i7-4900mq-ram-16gb-ssd-256gb-hdd-500gb-quadro-k2100m-fhd-pd44026.html',
    urlnew: '/dell-precision-m4800',
  },
  {
    urlold:
      '/dell-precision-m4800/dell-precision-m4800-i7-4900mq-ram-16gb-ssd-512gb-quadro-k2100m-fhd-pd44024.html',
    urlnew: '/dell-precision-m4800',
  },
  {
    urlold:
      '/dell-precision-m4800/dell-precision-m4800-i7-4900mq-ram-8gb-hdd-500gb-fhd-quadro-k2100m-pd44025.html',
    urlnew: '/dell-precision-m4800',
  },
  {
    urlold: '/dell-precision-3000-series-pl529.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/inspiron-pl269.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-inspiron-7590-pl564.html',
    urlnew: '/dell-inspiron-7590',
  },
  {
    urlold:
      '/dell-inspiron-7590/dell-inspiron-7590-i5-9300h-ram-8gb-ssd-256gb-fhd-gtx1050-pd44132.html',
    urlnew: '/dell-inspiron-7590',
  },
  {
    urlold: '/dell-inspiron-7370-pl497.html',
    urlnew: '/dell-inspirion-7370',
  },
  {
    urlold: '/dell-inspiron-7570-pl313.html',
    urlnew: '/dell-inspiron-7570',
  },
  {
    urlold:
      '/dell-inspiron-7570/dell-inspiron-7570-i7-8550u-ram-8gb-sshd-1tb-fhd-touch-mx130-pd43700.html',
    urlnew: '/dell-inspiron-7570',
  },
  {
    urlold:
      '/dell-inspiron-7570/dell-inspiron-7570-i7-8550u-ram-8gb-sshd-1tb-fhd-touch-940mx-pd43866.html',
    urlnew: '/dell-inspiron-7570',
  },
  {
    urlold:
      '/dell-inspiron-7570/dell-inspiron-7570-i7-8550u-ram-16gb-256gb-1tb-fhd-touch-mx130-pd43872.html',
    urlnew: '/dell-inspiron-7570',
  },
  {
    urlold:
      '/dell-inspiron-7570/dell-inspiron-7570-i7-8550u-ram-8gb-128gb-1tb-fhd-mx130-pd43871.html',
    urlnew: '/dell-inspiron-7570',
  },
  {
    urlold: '/dell-inspiron-7577-pl270.html',
    urlnew: '/dell-inspiron-7577',
  },
  {
    urlold: '/alienware-pl232.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-alienware-13-pl436.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold:
      '/dell-alienware-13/dell-alienware-13-r3-oled-i7-7700hq-ram-16gb-ssd-256gb-pd43753.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-alienware-m15-pl449.html',
    urlnew: '/dell-alienware-m15-r2',
  },
  {
    urlold: '/dell-alienware-m15-r2-pl501.html',
    urlnew: '/dell-alienware-m15-r2',
  },
  {
    urlold:
      '/dell-alienware-m15-r2/dell-alienware-m15-r2-i7-9750h-ram-16gb-ssd-1tb-fhd-ips-240hz-rtx-2070-pd43998.html',
    urlnew: '/dell-alienware-m15-r2',
  },
  {
    urlold:
      '/dell-alienware-m15-r2/dell-alienware-m15-r2-i7-9750h-ram-16gb-ssd-512gb-fhd-ips-rtx-2060-pd43950.html',
    urlnew: '/dell-alienware-m15-r2',
  },
  {
    urlold: '/dell-alienware-15-pl522.html',
    urlnew: '/dell-alienware-15-r2',
  },
  {
    urlold:
      '/dell-alienware-15-r2/dell-alienware-15-r2-i7-6700hq-ram-16gb-ssd-1tb-gtx-970m-fhd-ips-pd43754.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-alienware-15-r2-pl261.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-alienware-15-r4-pl304.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold:
      '/dell-alienware-15-r4/dell-alienware-15-r4-i7-8750h-ram-16gb-ssd-128gb-hdd-1tb-fhd-ips-gtx-1060-pd43599.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-alienware-17-r5-pl305.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold:
      '/dell-alienware-area-51m/dell-alienware-area-51m-i7-9700k-ram-16gb-ssd-256gb-rtx-2060-fhd-ips-pd43839.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-alienware-area-51m-pl478.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-latitude-pl229.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-latitude-pl229.html?sort=4',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold:
      '/dell-latitude-5400/latitude-5400-i5-8365u-ram-8gb-ssd-512gb-14-fhd-ips-pd44221',
    urlnew: '/dell-latitude-5400',
  },
  {
    urlold: '/dell-latitude-7300-pl557.html',
    urlnew: '/dell-latitude-7300',
  },
  {
    urlold:
      '/dell-latitude-7300/dell-latitude-7300-i5-8365u-ram-8gb-ssd-256gb-fhd-ips-pd44100.html',
    urlnew: '/dell-latitude-7300',
  },
  {
    urlold: '/dell-latitude-7200-pl506.html',
    urlnew: '/dell-latitude-7200',
  },
  {
    urlold:
      '/dell-latitude-7200/dell-latitude-7200-2-in-1-i5-8365u-ram-8gb-ssd-256gb-fhd-touch-pd43961.html',
    urlnew: '/dell-latitude-7200',
  },
  {
    urlold:
      '/dell-latitude-7480/dell-latitude-7480-i5-6300u-ram-8gb-ssd-256gb-fhd-ips-pd43474.html',
    urlnew: '/dell-latitude-7480',
  },
  {
    urlold:
      '/dell-latitude-7480/dell-latitude-7480-i7-7600u-ram-16gb-ssd-256gb-fhd-ips-pd43604.html',
    urlnew: '/dell-latitude-7480',
  },
  {
    urlold:
      '/dell-latitude-7480/dell-latitude-7480-i5-7200u-ram-8gb-ssd-256gb-fhd-ips-pd43945.html',
    urlnew: '/dell-latitude-7480',
  },
  {
    urlold:
      '/dell-latitude-7480/dell-latitude-7480-i5-6300u-ram-16gb-ssd-512gb-fhd-ips-touch-pd43946.html',
    urlnew: '/dell-latitude-7480',
  },
  {
    urlold:
      '/dell-latitude-7480/dell-latitude-7480-i7-6600u-ram-16gb-ssd-256gb-fhd-ips-pd43768.html',
    urlnew: '/dell-latitude-7480',
  },
  {
    urlold: '/dell-latitude-7480-pl362.html',
    urlnew: '/dell-latitude-7480',
  },
  {
    urlold: '/dell-latitude-7390-pl375.html',
    urlnew: '/dell-latitude-7390',
  },
  {
    urlold:
      '/dell-latitude-7390/dell-latitude-7390-i5-8350u-ram-8gb-ssd-256gb-fhd-ips-touch-pd43666.html',
    urlnew: '/dell-latitude-7390',
  },
  {
    urlold: '/dell-latitude-7400-pl556.html',
    urlnew: '/dell-latitude-7400',
  },
  {
    urlold:
      '/dell-latitude-7400/dell-latitude-7400-i5-8265u-ram-8gb-ssd-256gb-fhd-ips-pd44105.html',
    urlnew: '/dell-latitude-7400',
  },
  {
    urlold:
      '/dell-latitude-7400/dell-latitude-7400-i5-8265u-ram-8gb-ssd-256gb-fhd-ips-pd44032.html',
    urlnew: '/dell-latitude-7400',
  },
  {
    urlold:
      '/dell-latitude-7400/dell-latitude-7400-i7-8665u-ram-8gb-ssd-256gb-fhd-pd44085.html',
    urlnew: '/dell-latitude-7400',
  },
  {
    urlold:
      '/dell-latitude-7400/dell-latitude-7400-i5-8365u-ram-8gb-ssd-256gb-hd-pd44067.html',
    urlnew: '/dell-latitude-7400',
  },
  {
    urlold:
      '/dell-latitude-7400/dell-latitude-7400-i5-8365u-ram-8gb-ssd-256gb-fhd-ips-pd44086.html',
    urlnew: '/dell-latitude-7400',
  },
  {
    urlold:
      '/dell-latitude-e5570/dell-latitude-e5570-i5-6440hq-ram-8gb-ssd-256gb-fhd-ips-pd43915.html',
    urlnew: '/dell-latitude-e5570',
  },
  {
    urlold:
      '/dell-latitude-e5570/dell-latitude-e5570-i5-6440hq-ram-16gb-ssd-512gb-fhd-ips-pd43917.html',
    urlnew: '/dell-latitude-e5570',
  },
  {
    urlold:
      '/dell-latitude-e5570/dell-latitude-e5570-i5-6440hq-ram-8gb-ssd-128gb-fhd-ips-pd43916.html',
    urlnew: '/dell-latitude-e5570',
  },
  {
    urlold:
      '/dell-latitude-e5570/dell-latitude-e5580-i7-7820hq-ram-8gb-ssd-256gb-fhd-ips-touch-pd43919.html',
    urlnew: '/dell-latitude-e5570',
  },
  {
    urlold:
      '/dell-latitude-e5570/dell-latitude-e5580-i7-7820hq-ram-32gb-ssd-512gb-fhd-ips-touch-pd43919.html',
    urlnew: '/dell-latitude-e5570',
  },
  {
    urlold: '/dell-latitude-7490-pl363.html',
    urlnew: '/dell-latitude-7490',
  },
  {
    urlold:
      '/dell-latitude-7490/dell-latitude-7490-i5-8350u-ram-8gb-ssd-256gb-fhd-pd43565.html',
    urlnew: '/dell-latitude-7490',
  },
  {
    urlold:
      '/dell-latitude-7490/dell-latitude-7490-i5-8250u-ram-8gb-ssd-256gb-fhd-pd43891.html',
    urlnew: '/dell-latitude-7490',
  },
  {
    urlold: '/dell-latitude-7000-series-pl342.html',
    urlnew: '/dell-latitude-7000',
  },
  {
    urlold:
      '/dell-latitude-7000-series/dell-latitude-7400-i5-8365u-ram-8gb-ssd-256gb-fhd-ips-pd44032.html',
    urlnew: '/dell-latitude-7000',
  },
  {
    urlold: '/dell-latitude-5500-pl528.html',
    urlnew: '/dell-latitude-5500',
  },
  {
    urlold:
      '/dell-latitude-5500/dell-latitude-5500-2019-i5-8365u-ram-8gb-ssd-256gb-15-6-fhd-ips-pd43994.html',
    urlnew: '/dell-latitude-5500',
  },
  {
    urlold: '/dell-latitude-e5470-pl495.html',
    urlnew: '/dell-latitude-e5470',
  },
  {
    urlold:
      '/dell-latitude-e5470/dell-latitude-e5470-i5-6300u-ram-8gb-ssd-256gb-fhd-pd44064.html',
    urlnew: '/dell-latitude-e5470',
  },
  {
    urlold:
      '/dell-latitude-e5470/dell-latitude-e5470-i7-6820hq-ram-16gb-ssd-512gb-fhd-touch-pd43920.html',
    urlnew: '/dell-latitude-e5470',
  },
  {
    urlold: '/dell-latitude-e7470-pl361.html',
    urlnew: '/dell-latitude-e7470',
  },
  {
    urlold:
      '/dell-latitude-e7470/dell-latitude-e7470-i5-6300u-ram-8gb-ssd-256gb-fhd-pd43284.html',
    urlnew: '/dell-latitude-e7470',
  },
  {
    urlold:
      '/dell-latitude-e7470/dell-latitude-e7470-i5-6300u-ram-8gb-ssd-128gb-fhd-ips-pd43911.html',
    urlnew: '/dell-latitude-e7470',
  },
  {
    urlold:
      '/dell-latitude-e7470/dell-latitude-e7470-i7-6600u-ram-16gb-ssd-512gb-fhd-ips-pd43914.html',
    urlnew: '/dell-latitude-e7470',
  },
  {
    urlold:
      '/dell-latitude-e7470/dell-latitude-e7470-i7-6600u-ram-8gb-ssd-256gb-fhd-ips-pd43912.html',
    urlnew: '/dell-latitude-e7470',
  },
  {
    urlold:
      '/dell-latitude-e7470/dell-latitude-e7470-i7-6600u-ram-16gb-ssd-256gb-fhd-ips-pd43913.html',
    urlnew: '/dell-latitude-e7470',
  },
  {
    urlold: '/dell-latitude-5000-series-pl341.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-latitude-5480-pl499.html',
    urlnew: '/dell-latitude-5480',
  },
  {
    urlold:
      '/dell-latitude-5480/dell-latitude-5480-i5-7300u-ram-8gb-ssd-256gb-hd-pd43940.html',
    urlnew: '/dell-latitude-5480',
  },
  {
    urlold: '/dell-g-series-pl299.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-g3-pl307.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-g5-2019-pl447.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-g5-pl308.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold:
      '/dell-g5-2019/dell-g5-5590-i7-9750h-ram-16gb-ssd-512gb-15-6-fhd-ips-144hz-pd44120.html',
    urlnew: '/dell-gaming-g5-5590',
  },
  {
    urlold:
      '/dell-g5-2019/dell-g5-15-5590-i5-9300h-ram-8gb-1tb-hdd-128gb-ssd-fhd-ips-gtx-1650-pd43774.html',
    urlnew: '/dell-gaming-g5-5590',
  },
  {
    urlold:
      '/dell-g5-2019/dell-g5-15-5590-i7-9750h-ram-8gb-ssd-128g-hdd-1tb-1660ti-fhd-ips-pd43952.html',
    urlnew: '/dell-gaming-g5-5590',
  },
  {
    urlold:
      '/dell-g7-2019/dell-g7-15-7590-i7-9750h-ram-16gb-ssd-256gb-hdd-1tb-fhd-ips-144hz-rtx-2060-pd43780.html',
    urlnew: '/dell-gaming-g7-7590',
  },
  {
    urlold:
      '/dell-g7-2019/dell-g7-15-7590-i7-9750h-ram-8gb-ssd-128gb-hdd-1tb-fhd-ips-gtx-1660ti-pd43870.html',
    urlnew: '/dell-gaming-g7-7590',
  },
  {
    urlold: '/dell-g7-2019-pl448.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-g7-pl310.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: 'MAC',
  },
  {
    urlold: '/apple-pl226.html',
    urlnew: '/thuong-hieu/apple',
  },
  {
    urlold: '/macbook-pro-13-pl517.html',
    urlnew: '/apple-macbook-pro-13-2017',
  },
  {
    urlold: '/macbook-pro-13-mr9q2-pl381.html',
    urlnew: '/apple-macbook-pro-13-2020',
  },
  {
    urlold: '/macbook-pro-13-2017-pl554.html',
    urlnew: '/apple-macbook-pro-13-2017',
  },
  {
    urlold:
      '/macbook-pro-13-2017/apple-macbook-pro-13-2017-mpxt2ll-a-r-i5-2-3ghz-ram-8gb-ssd-256gb-13-3-pd44087.html',
    urlnew: '/apple-macbook-pro-13-2017',
  },
  {
    urlold:
      '/macbook-pro-13-2017/apple-macbook-pro-13-2017-5pxtll-a-i5-2-3ghz-ram-8gb-ssd-256gb-13-3-pd44087.html',
    urlnew: '/apple-macbook-pro-13-2017',
  },
  {
    urlold:
      '/macbook-pro-13-2017/apple-macbook-pro-13-2017-5pxt2ll-a-r-i5-2-3ghz-ram-8gb-ssd-256gb-touchbar-pd44088.html',
    urlnew: '/apple-macbook-pro-13-2017',
  },
  {
    urlold: '/macbook-pro-13-2018-pl520.html',
    urlnew: '/apple-macbook-pro-13-2018',
  },
  {
    urlold:
      '/macbook-pro-13-mr9q2/macbook-pro-13-2018-touchbar-core-i5-ram-8gb-ssd-256gb-pd43811.html',
    urlnew: '/apple-macbook-pro-13-2018',
  },
  {
    urlold: '/macbook-pro-15-pl515.html',
    urlnew: '/apple-macbook-pro-15-2015',
  },
  {
    urlold: '/macbook-pro-15-mlh42-pl380.html',
    urlnew: '/apple-macbook-pro-15-2015',
  },
  {
    urlold: '/macbook-pro-15-2016-pl518.html',
    urlnew: '/apple-macbook-pro-15-2016',
  },
  {
    urlold:
      '/macbook-pro-15-2018-mr932/macbook-pro-15-2018-mr932-touchbar-i7-ram-16gb-ssd-256gb-amd-555x-pd43673.html',
    urlnew: '/apple-macbook-pro-15-2018',
  },
  {
    urlold: '/macbook-pro-16-pl561.html',
    urlnew: '/apple-macbook-pro-16-2019',
  },
  {
    urlold: '/macbook-air-pl519.html',
    urlnew: '/apple-macbook-air-2020',
  },
  {
    urlold:
      '/macbook-air-2020/macbook-air-2020-i3-1-1ghz-ram-8gb-ssd-256gb-13-retina-2-560x1-600-touchid-pd43749.html',
    urlnew: '/apple-macbook-air-2020',
  },
  {
    urlold:
      '/macbook-air-2020/macbook-air-2020-i5-1-1ghz-ram-8gb-ssd-512gb-13-retina-2-560x1-600-touchid-pd44128.html',
    urlnew: '/apple-macbook-air-2020',
  },
  {
    urlold: '/imac-pl532.html',
    urlnew: '/thuong-hieu/apple',
  },
  {
    urlold:
      '/imac/imac-pro-2019-xeon-3-2ghz-ram-32gb-1tb-amd-vega-56-27inch-retina-5k-pd44005.html',
    urlnew: '/thuong-hieu/apple',
  },
  {
    urlold:
      '/imac/imac-2019-i5-2-3ghz-ram-8gb-1tb-21-5-inch-fhd-led-backlit-pd43999.html',
    urlnew: '/thuong-hieu/apple',
  },
  {
    urlold: 'HP',
  },
  {
    urlold: '/hp-pl217.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold: '/hp-pl217.html?sort=4',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold: '/hp-envy-pl291.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold:
      '/hp-envy-13-2018/hp-envy-13-i5-8250u-ram-8gb-ssd-256gb-fhd-ips-pd43679.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold:
      '/hp-envy-13-2018/hp-envy-13-i5-8250u-ram-8gb-ssd-256gb-fhd-ips-pd43540.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold: '/hp-envy-13-2018-pl359.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold:
      '/hp-envy-13-2018/hp-envy-13-i5-8250u-ram-8gb-ssd-128gb-fhd-ips-pd43679.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold:
      '/hp-envy-15-x360/hp-envy-15-x360-r5-2500u-ram-8gb-ssd-128gb-fhd-ips-touch-pd43978.html',
    urlnew: '/hp-envy-15-x360',
  },
  {
    urlold: '/hp-zbook-pl242.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold: '/hp-zbook-14u-g6-pl560.html',
    urlnew: '/hp-zbook-14u-g6',
  },
  {
    urlold:
      '/hp-zbook-14u-g6/hp-zbook-14u-g6-i5-8365u-ssd-256gb-ram-8gb-fhd-touch-pd44074.html',
    urlnew: '/hp-zbook-14u-g6',
  },
  {
    urlold: '/hp-zbook-15-g3-pl264.html',
    urlnew: '/hp-zbook-15-g3',
  },
  {
    urlold:
      '/hp-zbook-15-g3/hp-zbook-15-g3-i7-6820hq-ram-16gb-ssd-256gb-m2000m-fhd-pd43867.html',
    urlnew: '/hp-zbook-15-g3',
  },
  {
    urlold:
      '/hp-zbook-15-g5/hp-zbook-15v-g5-i5-8300h-ram-16gb-ssd-256gb-fhd-ips-pd44061.html',
    urlnew: '/hp-zbook-15-g5',
  },
  {
    urlold:
      '/hp-zbook-15-g5/hp-zbook-15-studio-x360-g5-i5-8300h-ssd-256gb-ram-8gb-nvidia-p1000-touch-pd44055.html',
    urlnew: '/hp-zbook-15-g5',
  },
  {
    urlold:
      '/hp-zbook-15-g5/hp-zbook-15-studio-x360-g5-i5-8300h-ssd-256gb-ram-8gb-fhd-touch-pd44055.html',
    urlnew: '/hp-zbook-15-g5',
  },
  {
    urlold:
      '/hp-zbook-15-g5/hp-zbook-15-g5-i7-8750h-ram-16gb-ssd-256gb-fhd-ips-p1000-pd43863.html',
    urlnew: '/hp-zbook-15-g5',
  },
  {
    urlold: '/hp-zbook-15-g5-pl442.html',
    urlnew: '/hp-zbook-15-g5',
  },
  {
    urlold: '/hp-zbook-studio-g3-pl262.html',
    urlnew: '/hp-zbook-studio-g3',
  },
  {
    urlold:
      '/hp-zbook-studio-g3/hp-zbook-studio-g3-core-i7-6700hq-ram-16gb-ssd-256gb-fhd-ips-m1000m-4gb-pd43450.html',
    urlnew: '/hp-zbook-studio-g3',
  },
  {
    urlold:
      '/hp-zbook-studio-g4/hp-zbook-studio-g4-i7-7700hq-ram-16gb-ssd-512-gb-fhd-quadro-m1200m-pd43513.html',
    urlnew: '/hp-zbook-studio-g4',
  },
  {
    urlold:
      '/hp-zbook-studio-g5/hp-zbook-studio-g5-i7-8850h-ram-16gb-ssd-512gb-fhd-ips-p1000-pd44106.html',
    urlnew: '/hp-zbook-studio-g5',
  },
  {
    urlold:
      '/hp-zbook-studio-g5/hp-zbook-studio-g5-xeon-e-2176m-ram-16gb-ssd-512gb-fhd-ips-p1000-pd44057.html',
    urlnew: '/hp-zbook-studio-g5',
  },
  {
    urlold:
      '/hp-zbook-studio-g5/hp-zbook-studio-g5-i7-8850h-ram-16gb-ssd-512gb-fhd-ips-p1000-pd43865.html',
    urlnew: '/hp-zbook-studio-g5',
  },
  {
    urlold: '/hp-zbook-studio-g5-pl487.html',
    urlnew: '/hp-zbook-studio-g5',
  },
  {
    urlold: '/hp-pavilion-15-gaming-pl393.html',
    urlnew: '/hp-pavilion-15-gaming',
  },
  {
    urlold:
      '/hp-pavilion-15-gaming/hp-pavilion-15-2018-i5-8300h-ram-8gb-nvidia-gtx1050ti-fhd-pd43745.html',
    urlnew: '/hp-pavilion-15-gaming',
  },
  {
    urlold:
      '/hp-pavilion-15-gaming/hp-pavilion-15-2018-i7-8750h-ram-8gb-nvidia-gtx1050ti-fhd-pd43758.html',
    urlnew: '/hp-pavilion-15-gaming',
  },
  {
    urlold: '/hp-spectre-pl302.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold: '/hp-spectre-x360-13-2019-pl507.html',
    urlnew: '/hp-spectre-13-x360-2019',
  },
  {
    urlold:
      '/hp-spectre-x360-13-2019/hp-spectre-x360-13-2019-i5-8265u-ssd-256gb-ram-8gb-fhd-ips-touch-pd43799.html',
    urlnew: '/hp-spectre-13-x360-2019',
  },
  {
    urlold:
      '/hp-spectre-x360-15/hp-spectre-15t-x360-bl100-i7-8550u-ram-12gb-ssd-360gb-nvme-4k-touch-nvidia-mx150-pd43625.html',
    urlnew: '/hp-spectre-15-x360',
  },
  {
    urlold: '/hp-elitebook-pl234.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold: '/hp-elitebook-840-g6-pl548.html',
    urlnew: '/hp-elitebook-840-g6',
  },
  {
    urlold:
      '/hp-elitebook-840-g6/hp-elitebook-840-g6-i5-8265u-ssd-256gb-32gb-optane-ram-8gb-fhd-pd44059.html',
    urlnew: '/hp-elitebook-840-g6',
  },
  {
    urlold:
      '/hp-elitebook-830-g5/hp-elitebook-830-g5-i3-8130u-ssd-128gb-ram-8gb-fhd-pd43935.html',
    urlnew: '/hp-elitebook-830-g5',
  },
  {
    urlold: '/hp-elitebook-x360-1030-g3-pl566.html',
    urlnew: '/hp-elitebook-x360-1030-g3',
  },
  {
    urlold: '/hp-probook-pl411.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold:
      '/hp-probook/hp-probook-440-g5-i5-8250u-ram-8gb-hdd-1tb-hd-pd43705.html',
    urlnew: '/hp-probook-440-g5',
  },
  {
    urlold: '/hp-omen-2018-pl358.html',
    urlnew: '/hp-omen-2018',
  },
  {
    urlold: 'RAZER',
  },
  {
    urlold: '/razer-pl227.html',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold: '/razer-pl227.html?sort=4',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold: '/razer-blade-pl274.html',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold:
      '/razer-blade/razer-blade-2017-i7-7700hq-ram-16gb-ssd-512gb-fhd-gtx-1060-6gb-pd43694.html',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold:
      '/razer-blade/razer-blade-2017-i7-7700hq-ram-16gb-ssd-512gb-fhd-gtx-1060-6gb-pd43452.html',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold: '/razer-blade-stealth-pl275.html',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold:
      '/razer-blade-stealth-12-5/razer-blade-stealth-2017-i7-7500u-ram-16gb-ssd-512gb-12-5-4k-uhd-pd43473.html',
    urlnew: '/razer-blade-stealth-125',
  },
  {
    urlold: '/razer-blade-stealth-13-3-pl344.html',
    urlnew: '/razer-blade-stealth-133',
  },
  {
    urlold:
      '/razer-blade-stealth-13-3/razer-blade-stealth-13-3-2019-i7-8565u-ram-16gb-ssd-256gb-mx150-fhd-ips-pd43848.html',
    urlnew: '/razer-blade-stealth-133',
  },
  {
    urlold:
      '/razer-blade-stealth-13-3/razer-blade-stealth-13-3-i7-1065g7-ram-16gb-ssd-512gb-gtx1650-fhd-ips-pd44031.html',
    urlnew: '/razer-blade-stealth-133',
  },
  {
    urlold:
      '/razer-blade-stealth-13-3/razer-blade-stealth-13-3-2019-i7-8565u-ram-16gb-ssd-256gb-mx150-fhd-ips-quartz-pink-pd44007.html',
    urlnew: '/razer-blade-stealth-133',
  },
  {
    urlold:
      '/razer-blade-stealth-13-3/razer-blade-stealth-13-3-i7-1065g7-ram-16gb-ssd-256gb-fhd-ips-pd44122.html',
    urlnew: '/razer-blade-stealth-133',
  },
  {
    urlold:
      '/razer-blade-stealth-13-3/razer-blade-stealth-quartz-pink-13-3-i7-1065g7-ram-16gb-ssd-512gb-gtx1650-fhd-ips-pd44103.html',
    urlnew: '/razer-blade-stealth-133',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-white-i7-8750h-ram-16gb-ssd-512gb-fhd-ips-144hz-rtx-2070-pd44043.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-i7-8750h-ram-16gb-ssd-256gb-fhd-ips-144hz-gtx-1070-pd43613.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-i7-9750h-ram-16gb-ssd-512gb-fhd-ips-144hz-rtx-2060-pd44092.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-i7-8750h-ram-16gb-ssd-512gb-fhd-gtx1060-pd43899.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-white-i7-9750h-ram-16gb-ssd-512gb-fhd-ips-144hz-rtx-2060-pd44056.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-mecury-white-i7-8750h-ram-16gb-ssd-512gb-fhd-ips-144hz-gtx1070-pd44096.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-i7-8750h-ram-16gb-ssd-512gb-fhd-ips-144hz-gtx-1070-pd43612.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-quartz-pink-i7-8750h-ram-16gb-ssd-512gb-fhd-ips-rtx-2060-pd44102.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-i7-8750h-ram-16gb-ssd-512gb-fhd-ips-144hz-rtx-2080-pd43802.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-i7-8750h-ram-16gb-ssd-512gb-fhd-ips-rtx-2060-early-2019-pd43791.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-i7-8750h-ram-32gb-ssd-512gb-4k-uhd-ips-144hz-rtx-2080-pd43790.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-15/razer-blade-15-i7-8750h-ram-32gb-ssd-1tb-fhd-ips-144hz-gtx-1070-pd43614.html',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-blade-pro/razer-blade-pro-17-gaming-i7-9750h-ssd-512gb-ram-16gb-17-3-fhd-144hz-nvidia-reg-rtx-2060-pd44019.html',
    urlnew: '/new-razer-blade-pro-17',
  },
  {
    urlold:
      '/razer-blade-pro/razer-blade-pro-i7-7700hq-ram-16gb-gtx-1060-6gb-120hz-pd43539.html',
    urlnew: '/new-razer-blade-pro-17',
  },
  {
    urlold: 'KHÁC',
  },
  {
    urlold: '/msi-gf63-pl416.html',
    urlnew: '/thuong-hieu/msi',
  },
  {
    urlold: '/surface-laptop-2-pl438.html',
    urlnew: '/surface-laptop-2-135',
  },
  {
    urlold:
      '/surface-laptop/surface-laptop-2-i5-8250u-8gb-256gb-ssd-fhd-cobalt-blue-pd44176.html',
    urlnew: '/surface-laptop-2-135',
  },
  {
    urlold:
      '/surface-laptop/surface-laptop-3-13-5-i5-10210u-8gb-256gb-ssd-fhd-cobalt-blue-pd44011.html',
    urlnew: '/surface-laptop-3-135',
  },
  {
    urlold:
      '/surface-book-2/surface-book-2-i7-8650u-ram-16gb-ssd-256gb-nvidia-gtx-1060-6gb-pd43667.html',
    urlnew: '/surface-book-2-15',
  },
  {
    urlold: '/asus-gaming-f571-pl527.html',
    urlnew: '/thuong-hieu/asus',
  },
  {
    urlold: '/asus-gaming-f571/asus-f571gd-bq319t-pd43988.html',
    urlnew: '/thuong-hieu/asus',
  },
  {
    urlold:
      '/asus-zenbook-pro-15-ux580gd/asus-zenbook-pro-ux580gd-i7-8750h-ram-16gb-ssd512gb-15-6-gtx1050-4gb-4k-led-backlit-pd44062.html',
    urlnew: '/thuong-hieu/asus',
  },
  {
    urlold:
      '/asus-expertbook-p5440/asus-expertbook-p5440-i5-8265u-ram-8g-ssd-512gb-fhd-pd43896.html',
    urlnew: '/asus-expertbook-p5440',
  },
  {
    urlold: '/asus-expertbook-p5440-pl493.html',
    urlnew: '/asus-expertbook-p5440',
  },
  {
    urlold: '/asus-pl429.html',
    urlnew: '/thuong-hieu/asus',
  },
  {
    urlold: '/asus-tuf-fx505-gaming/asus-fx505gd-bq325t-pd43738.html',
    urlnew: '/asus-tuf-fx505-gaming',
  },
  {
    urlold: '/lg-gram-2020-pl567.html',
    urlnew: '/lg-gram-2020',
  },
  {
    urlold: '/acer-nitro-5-pl576.html',
    urlnew: '/acer-nitro-5',
  },
  {
    urlold: '/egpu-razer-core-x-pl474.html',
    urlnew: '/razer-core-x',
  },
  {
    urlold:
      '/egpu-razer-core-x/razer-core-x-chroma-thunderbolt-3-cho-laptop-cao-cap-700w-pd44042.html',
    urlnew: '/razer-core-x-chroma',
  },
  {
    urlold: 'CHUNG',
  },
  {
    urlold: '/san-pham.html?budget=155',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?budget=154',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?use=188',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?budget=156',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?use=203',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?use=207',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/dell-precision-5510-738.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/alienware-15-r2-838.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/precision-m4800-2034.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?use=186',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/dell-xps-15-9560-921.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/thinkpad-p50-985.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?budget=157',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?use=188?utm_source=ThinkViewY',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?use=325',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?cate_status=201',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/ban-dell-chinh-hang-1196.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/dell-inspiron-7570-thinkpro-3353.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/ban-dell-nhap-khau-1486.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/dell-inspiron-5577-2140.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?use=188&sort=4',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?use=187',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/dell-precision-5520-841.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?budget=322',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?budget=321',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/dell-precision-7510-722.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham/tag/dell-xps-15-9570-thinkpro-3005.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?sort=4',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/san-pham.html?use=189',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=7480',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=Macbook',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=macbook',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=Dell',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=Razer',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=Dell+xps',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=Thinkpad',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=thinkpad',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=7510',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=t450s',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=7590',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=xps',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=720s',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=Xps',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=XPS+13+9360',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=Asus',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=5510',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/product/search?keyword=precision+5530',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold:
      '/tin-khuyen-mai/deal-cuc-soc-dau-nam-giam-gia-den-15-trieu-dong-nd498460.html',
    urlnew:
      '/tin-tuc/khuyen-mai/deal-cuc-soc-dau-nam-giam-gia-den-15-trieu-dong',
  },
  {
    urlold: '/tin-khuyen-mai-nl70.html',
    urlnew: '/tin-tuc/khuyen-mai',
  },
  {
    urlold: '/tin-tuc/tag/thinkpad-x1-carbon-gen-5-2486.html',
    urlnew: '/tin-tuc/tin-tuc/thinkpad-x1-carbon-gen-5',
  },
  {
    urlold: '/tin-tuc-nl62.html',
    urlnew: '/tin-tuc/tin-tuc',
  },
  {
    urlold:
      '/tin-tuc/dell-precision-7510-bao-hanh-dell-chinh-hang-viet-nam-tai-nha-nd497715.html',
    urlnew:
      '/tin-tuc/tin-tuc/thinkpad-x1-carbon-gen-5/dell-precision-7510-bao-hanh-dell-chinh-hang-viet-nam-tai-nha',
  },
  {
    urlold:
      '/tin-cong-nghe/cau-hinh-may-tinh-can-thiet-de-choi-lien-minh-huyen-thoai-lol-nd498043.html',
    urlnew:
      '/tin-tuc/tin-tuc/cau-hinh-may-tinh-can-thiet-de-choi-lien-minh-huyen-thoai-lol',
  },
  {
    urlold:
      '/tin-cong-nghe/tai-sao-ban-phim-phai-la-qwerty-ma-khong-phai-la-abcdef-nd498584.html',
    urlnew:
      '/tin-tuc/tin-tuc/tai-sao-ban-phim-phai-la-qwerty-ma-khong-phai-la-abcdef',
  },
  {
    urlold:
      '/tin-cong-nghe/2-4-ghz-va-5-ghz-la-gi-su-khac-nhau-giua-wi-fi-2-4-ghz-va-5-ghz-nd498166.html',
    urlnew:
      '/tin-tuc/tin-tuc/2-4-ghz-va-5-ghz-la-gi-su-khac-nhau-giua-wi-fi-2-4-ghz-va-5-ghz',
  },
  {
    urlold: '/tin-cong-nghe/cam-nhan-co-ban-ve-dell-xps-13-2020-nd498468.html',
    urlnew: '/tin-tuc/tin-tuc/cam-nhan-co-ban-ve-dell-xps-13-2020',
  },
  {
    urlold:
      '/tin-cong-nghe/dell-xps-15-9500-ma-nh-hon-7590-nhung-va-n-chua-ba-ng-9700-nd498609.html',
    urlnew:
      '/tin-tuc/tin-tuc/dell-xps-15-9500-ma-nh-hon-7590-nhung-va-n-chua-ba-ng-9700',
  },
  {
    urlold:
      '/tin-cong-nghe/card-do-hoa-gtx-1650-se-co-hieu-nang-tuong-duong-1050ti-nd497927.html',
    urlnew:
      '/tin-tuc/tin-tuc/card-do-hoa-gtx-1650-se-co-hieu-nang-tuong-duong-1050ti',
  },
  {
    urlold:
      '/tin-cong-nghe/cau-hinh-may-tinh-can-thiet-de-choi-lien-minh-huyen-thoai-lol-nd498043.html',
    urlnew:
      '/tin-tuc/tin-tuc/cau-hinh-may-tinh-can-thiet-de-choi-lien-minh-huyen-thoai-lol',
  },
  {
    urlold:
      '/tin-cong-nghe/asus-rog-zephyrus-g14-laptop-gaming-chat-nd498477.html',
    urlnew: '/tin-tuc/tin-tuc/asus-rog-zephyrus-g14-laptop-gaming-chat',
  },
  {
    urlold:
      '/tin-cong-nghe/xiaomi-mi-air-2s-ban-sao-gia-re-cua-apple-airpods-nd498606.html',
    urlnew:
      '/tin-tuc/tin-tuc/xiaomi-mi-air-2s-ban-sao-gia-re-cua-apple-airpods',
  },
  {
    urlold:
      '/tin-cong-nghe/huong-dan-check-bao-hanh-may-cac-hang-laptop-nd497858.html',
    urlnew: '/tin-tuc/tin-tuc/huong-dan-check-bao-hanh-may-cac-hang-laptop',
  },
  {
    urlold:
      '/tin-cong-nghe/tai-sao-chung-ta-phai-dung-windows-ban-quyen-thay-window-ldquo-lau-nd498551.html',
    urlnew:
      '/tin-tuc/tin-tuc/tai-sao-chung-ta-phai-dung-windows-ban-quyen-thay-window-ldquo-lau',
  },
  {
    urlold:
      '/tin-cong-nghe/nintendo-switch-pro-se-ra-mat-trong-nam-nay-nd498489.html',
    urlnew: '/tin-tuc/tin-tuc/nintendo-switch-pro-se-ra-mat-trong-nam-nay',
  },
  {
    urlold:
      '/tin-cong-nghe/apple-bat-ngo-danh-up-nguoi-dung-voi-ipad-pro-2020-nd498601.html',
    urlnew:
      '/tin-tuc/tin-tuc/apple-bat-ngo-danh-up-nguoi-dung-voi-ipad-pro-2020',
  },
  {
    urlold: '/dan-cong-nghe/cac-dong-laptop-dell-hien-nay-nd497742.html',
    urlnew: '/tin-tuc/tin-tuc/cac-dong-laptop-dell-hien-nay-nd497742.html',
  },
  {
    urlold:
      '/dan-cong-nghe/cach-kiem-tra-laptop-cai-win-ban-quyen-hay-khong-nd497750.html',
    urlnew: '/tin-tuc/tin-tuc/cach-kiem-tra-laptop-cai-win-ban-quyen-hay-khong',
  },
  {
    urlold:
      '/dan-cong-nghe/cach-lua-chon-cpu-laptop-phu-hop-nhu-cau-nd497740.html',
    urlnew: '/tin-tuc/tin-tuc/cach-lua-chon-cpu-laptop-phu-hop-nhu-cau',
  },
  {
    urlold: '/dan-cong-nghe/cac-dong-laptop-lenovo-nd497746.html',
    urlnew: '/tin-tuc/tin-tuc/cac-dong-laptop-lenovo',
  },
  {
    urlold:
      '/dan-cong-nghe/hien-tuong-o-cung-tu-day-va-cach-khac-phuc-nd497909.html',
    urlnew: '/tin-tuc/tin-tuc/hien-tuong-o-cung-tu-day-va-cach-khac-phuc',
  },
  {
    urlold:
      '/dan-cong-nghe/nhung-phan-mem-kiem-tra-may-tinh-chat-luong-nhat-hien-nay-nd497763.html',
    urlnew:
      '/tin-tuc/tin-tuc/nhung-phan-mem-kiem-tra-may-tinh-chat-luong-nhat-hien-nay',
  },
  {
    urlold: '/dan-cong-nghe/laptop-hp-nao-danh-cho-ban-nd497745.html',
    urlnew: '/tin-tuc/tin-tuc/laptop-hp-nao-danh-cho-ban',
  },
  {
    urlold:
      '/dan-cong-nghe/cach-chia-lai-o-cung-khong-mat-du-lieu-nd497760.html',
    urlnew: '/tin-tuc/tin-tuc/cach-chia-lai-o-cung-khong-mat-du-lieu',
  },
  {
    urlold:
      '/dan-cong-nghe/co-nen-dung-sac-du-phong-khong-10-tac-dung-va-luu-y-quan-trong-can-nho-nd498356.html',
    urlnew:
      '/tin-tuc/tin-tuc/co-nen-dung-sac-du-phong-khong-10-tac-dung-va-luu-y-quan-trong-can-nho',
  },
  {
    urlold:
      '/dan-cong-nghe/huong-dan-su-dung-afterburner-de-hien-thong-so-cpu-gpu-trong-game-nd497753.html',
    urlnew:
      '/tin-tuc/tin-tuc/huong-dan-su-dung-afterburner-de-hien-thong-so-cpu-gpu-trong-game',
  },
  {
    urlold:
      '/dan-cong-nghe/khai-niem-o-cung-o-cung-ssd-va-hdd-la-gi-nd497741.html',
    urlnew: '/tin-tuc/tin-tuc/khai-niem-o-cung-o-cung-ssd-va-hdd-la-gi',
  },
  {
    urlold:
      '/dan-cong-nghe/cach-truy-cap-bios-cua-mot-so-hang-may-nd497979.html',
    urlnew: '/tin-tuc/tin-tuc/cach-truy-cap-bios-cua-mot-so-hang-may',
  },
  {
    urlold:
      '/dan-cong-nghe/tat-che-do-tu-tat-man-hinh-cua-laptop-nd498028.html',
    urlnew: '/tin-tuc/tin-tuc/tat-che-do-tu-tat-man-hinh-cua-laptop',
  },
  {
    urlold:
      '/dan-cong-nghe/lieu-windows-10-co-can-phai-dung-cac-phan-mem-diet-virus-khac-khong-nd497967.html',
    urlnew:
      '/tin-tuc/tin-tuc/lieu-windows-10-co-can-phai-dung-cac-phan-mem-diet-virus-khac-khong',
  },
  {
    urlold:
      '/dan-cong-nghe/cach-doi-vi-tri-2-nut-fn-va-ctrl-cua-thinkpad-nd497978.html',
    urlnew: '/tin-tuc/tin-tuc/cach-doi-vi-tri-2-nut-fn-va-ctrl-cua-thinkpad',
  },
  {
    urlold: '/dan-cong-nghe/vga-la-gi-tim-hieu-vga-laptop-nd497747.html',
    urlnew: '/tin-tuc/tin-tuc/vga-la-gi-tim-hieu-vga-laptop',
  },
  {
    urlold:
      '/danh-gia/voi-15-trieu-ban-se-mua-cho-minh-chiec-laptop-nhu-the-nao-nd497766.html',
    urlnew:
      '/tin-tuc/review/voi-15-trieu-ban-se-mua-cho-minh-chiec-laptop-nhu-the-nao',
  },
  {
    urlold:
      '/danh-gia/dell-latitude-7390-chiec-laptop-13-inch-the-he-moi-cua-dell-nd497860.html',
    urlnew:
      '/tin-tuc/review/dell-latitude-7390-chiec-laptop-13-inch-the-he-moi-cua-dell',
  },
  {
    urlold: '/danh-gia/lenovo-thinkpad-e480-dong-thinkpad-gia-re-nd497840.html',
    urlnew: '/tin-tuc/review/lenovo-thinkpad-e480-dong-thinkpad-gia-re',
  },
  {
    urlold:
      '/danh-gia/danh-gia-dell-latitude-7480-laptop-doanh-nhan-hoan-hao-nhat-nd497729.html',
    urlnew:
      '/tin-tuc/review/danh-gia-dell-latitude-7480-laptop-doanh-nhan-hoan-hao-nhat',
  },
  {
    urlold:
      '/danh-gia/lenovo-thinkpad-t480s-giac-mo-cua-nguoi-quan-ly-cntt-nd497836.html',
    urlnew: '/tin-tuc/review/thinkpad-t480s-giac-mo-cua-nguoi-quan-ly-cntt',
  },
  {
    urlold:
      '/danh-gia/lenovo-thinkpad-x280-laptop-mong-nhat-va-nhe-nhat-the-he-x200-nd497833.html',
    urlnew:
      '/tin-tuc/review/lenovo-thinkpad-x280-laptop-mong-nhat-va-nhe-nhat-the-he-x200',
  },
  {
    urlold:
      '/danh-gia/so-sanh-intel-core-i7-8750h-va-core-i7-9750h-nd497968.html',
    urlnew: '/tin-tuc/review/so-sanh-intel-core-i7-8750h-va-core-i7-9750h',
  },
  {
    urlold:
      '/danh-gia/dell-precision-5520-suc-manh-cua-may-tram-voi-than-hinh-cua-mot-ultrabook-cao-cap-nd497770.html',
    urlnew:
      '/tin-tuc/review/dell-precision-5520-suc-manh-cua-may-tram-voi-than-hinh-cua-mot-ultrabook-cao-cap',
  },
  {
    urlold:
      '/danh-gia/hp-zbook-studio-g3-may-tram-cho-nhung-nguoi-thich-nhe-nhang-thanh-lich-nd497712.html',
    urlnew:
      '/tin-tuc/review/hp-zbook-studio-g3-may-tram-cho-nhung-nguoi-thich-nhe-nhang-thanh-lich',
  },
  {
    urlold:
      '/danh-gia/microsoft-surface-laptop-3-hoan-hao-cho-lam-viec-hieu-qua-nd498450.html',
    urlnew:
      '/tin-tuc/review/microsoft-surface-laptop-3-hoan-hao-cho-lam-viec-hieu-qua',
  },
  {
    urlold:
      '/danh-gia/top-laptop-duoi-10-trieu-cho-sinh-vien-lap-trinh-nd498060.html',
    urlnew: '/tin-tuc/review/top-laptop-duoi-10-trieu-cho-sinh-vien-lap-trinh',
  },
  {
    urlold:
      '/danh-gia/dell-xps-13-9360-mot-chiec-may-tinh-xach-tay-tuyet-voi-nd497781.html',
    urlnew:
      '/tin-tuc/review/dell-xps-13-9360-mot-chiec-may-tinh-xach-tay-tuyet-voi-',
  },
  {
    urlold: '/danh-gia/i7-8550u-hieu-nang-manh-me-nd497832.html',
    urlnew: '/tin-tuc/review/i7-8550u-hieu-nang-manh-me',
  },
  {
    urlold: '/danh-gia/danh-gia-chi-tiet-dell-alienware-17-r4-nd497824.html',
    urlnew: '/tin-tuc/review/danh-gia-chi-tiet-dell-alienware',
  },
  {
    urlold:
      '/danh-gia/dell-inspiron-5577-laptop-choi-game-tot-nhat-trong-tam-gia-nd497778.html',
    urlnew:
      '/tin-tuc/review/dell-inspiron-5577-laptop-choi-game-tot-nhat-trong-tam-gia',
  },
  {
    urlold:
      '/danh-gia/dragon-ball-z-kakarot-hanh-trinh-truy-tim-7-vien-ngoc-rong-nd498508.html',
    urlnew:
      '/tin-tuc/review/dragon-ball-z-kakarot-hanh-trinh-truy-tim-7-vien-ngoc-rong',
  },
  {
    urlold:
      '/danh-gia/can-canh-razer-blade-stealth-quartz-pink-dang-yeu-den-la-thuong-nd498533.html',
    urlnew:
      '/tin-tuc/review/can-canh-razer-blade-stealth-quartz-pink-dang-yeu-den-la-thuong',
  },
  {
    urlold: '/danh-gia/thinkpad-w540-ben-bi-theo-thoi-gian-nd498573.html',
    urlnew: '/tin-tuc/review/thinkpad-w540-ben-bi-theo-thoi-gian',
  },
  {
    urlold: '/danh-gia/thinkpad-t470s-laptop-cua-doanh-nhan-nd497827.html',
    urlnew: '/tin-tuc/review/thinkpad-t470s-laptop-cua-doanh-nhan',
  },
  {
    urlold:
      '/danh-gia/thinkpad-x270-laptop-sieu-di-dong-pin-len-toi-21-gio-danh-cho-doanh-nghiep-nd497791.html',
    urlnew:
      '/tin-tuc/review/thinkpad-x270-laptop-sieu-di-dong-pin-len-toi-21-gio-danh-cho-doanh-nghiep',
  },
  {
    urlold:
      '/danh-gia/dell-xps-13-9365-laptop-2-trong-1-dang-cap-doanh-nhan-nd497719.html',
    urlnew:
      '/tin-tuc/review/dell-xps-13-9365-laptop-2-trong-1-dang-cap-doanh-nhan',
  },
  {
    urlold:
      '/danh-gia/lenovo-legion-y545-ngon-nhung-it-ai-de-y-den-nd498484.html',
    urlnew: '/tin-tuc/review/lenovo-legion-y545-ngon-nhung-it-ai-de-y-den',
  },
  {
    urlold:
      '/chinh-sach-chung/chinh-sach-doi-tra-san-pham-tai-thinkpro-nd497705.html',
    urlnew: '/chinh-sach/chinh-sach-doi-tra-san-pham-tai-thinkpro',
  },
  {
    urlold: '/chinh-sach-chung/chinh-sach-bao-hanh-tai-thinkpro-nd497703.html',
    urlnew: '/chinh-sach/chinh-sach-doi-tra-san-pham-tai-thinkpro',
  },
  {
    urlold:
      '/san-pham-moi/elitebook-800-va-805-ra-mat-duoc-trang-bi-amd-ryzen-4000-nd498613.html',
    urlnew:
      '/tin-tuc/tin-tuc/elitebook-800-va-805-ra-mat-duoc-trang-bi-amd-ryzen-4000-nd498613',
  },
  {
    urlold: '/phu-kien/anker-premium-7-in-1-usb-c-hub-adapter-pd44022.html',
    urlnew: '/tin-tuc/gear/anker-premium-7-in-1-usb-c-hub-adapter',
  },
  {
    urlold: '/hang-chinh-hang-pl389.html',
    urlnew: '/hang-chinh-hang',
  },
  {
    urlold: '/hang-chinh-hang-pl389.html?sort=4',
    urlnew: '/hang-chinh-hang',
  },
  {
    urlold: '/hang-chinh-hang-pl389.html?company=401',
    urlnew: '/hang-chinh-hang',
  },
  {
    urlold: '/hang-chinh-hang-pl389.html?company=147',
    urlnew: '/hang-chinh-hang',
  },
  {
    urlold: '/hang-chinh-hang-pl389.html?company=144',
    urlnew: '/hang-chinh-hang',
  },
  {
    urlold: '/hang-chinh-hang-pl389.html?budget=155',
    urlnew: '/hang-chinh-hang',
  },
  {
    urlold: '/hang-chinh-hang-pl389.html?company=146',
    urlnew: '/hang-chinh-hang',
  },
  {
    urlold: '/hang-nhap-khau-pl388.html?company=145',
    urlnew: '/hang-nhap-khau',
  },
  {
    urlold: '/hang-nhap-khau-pl388.html?budget=155',
    urlnew: '/hang-nhap-khau',
  },
  {
    urlold: '/hang-nhap-khau-pl388.html',
    urlnew: '/hang-nhap-khau',
  },
  {
    urlold: '/hang-nhap-khau-pl388.html?sort=4',
    urlnew: '/hang-nhap-khau',
  },
  {
    urlold: '/hang-nhap-khau-pl388.html?company=144',
    urlnew: '/hang-nhap-khau',
  },
  {
    urlold: '/hang-nhap-khau-pl388.html?budget=154',
    urlnew: '/hang-nhap-khau',
  },
  {
    urlold: '/hang-moi-pl385.html',
    urlnew: '/hang-nhap-khau',
  },
  {
    urlold: '/may-tinh-de-ban-pl243.html',
    urlnew: '/pc',
  },
  {
    urlold:
      '/may-tinh-de-ban/razer-raptor-27inch-wqhd-144hz-non-glare-ips-pd44014.html',
    urlnew: '/pc',
  },
  {
    urlold: '/laptop-gaming-pl239.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-gaming-pl239.html?sort=4',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-gaming-pl239.html?budget=155',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-gaming-pl239.html?budget=154',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-gaming-pl239.html?budget=156',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-do-hoa-pl240.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-do-hoa-pl240.html?budget=155',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-do-hoa-pl240.html?sort=4',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/giam-gia-pl386.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/giam-gia-pl386.html?sort=4',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/giam-gia-pl386.html?budget=155',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-pl215.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-doanh-nhan-ultrabook-pl364.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/order',
    urlnew: '/gio-hang',
  },
  {
    urlold: '/order/tragop',
    urlnew: '/gio-hang',
  },
  {
    urlold:
      '/chinh-sach-chung/ho-tro-tra-gop-thinkpro-ho-tro-tra-gop-cho-toan-bo-san-pham-tai-cua-hang-khong-bat-buoc-phai-tra-truoc-nd497733.html',
    urlnew:
      '/tin-tuc/ve-thinkpro/chinh-sach-thanh-toan-va-van-chuyen-tai-thinkpro',
  },
  {
    urlold: '/phu-kien/balo-thinkpro-pd43648.html',
    urlnew: '/phu-kien',
  },
  {
    urlold: '/phu-kien-pl182.html',
    urlnew: '/phu-kien',
  },
  {
    urlold: '/tu-van-nl63.html',
    urlnew: '/tu-van',
  },
  {
    urlold: '/laptop-cu-pl294.html?sort=4',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-cu-pl294.html?company=145',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-cu-pl294.html',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-cu-pl294.html?budget=154',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-cu-pl294.html?company=144',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-cu-pl294.html?company=147',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-cu-pl294.html?budget=155',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/laptop-cu-pl294.html?use=188',
    urlnew: '/may-tinh-xach-tay',
  },
  {
    urlold: '/lg-gram-2020-pl567.html',
    urlnew: '/phu-kien',
  },
  {
    urlold: '/tuyen-dung-nl72.html',
    urlnew: '/tin-tuc/tuyen-dung',
  },
  {
    urlold: '/ssd-pl282.html',
    urlnew: '/ssd',
  },
  {
    urlold: '/ram-pl284.html',
    urlnew: '/ram',
  },
  {
    urlold:
      '/macbook-pro-13-2017/apple-macbook-pro-13-2017-mpxt2ll-a-r-i5-2-3ghz-ram-8gb-ssd-256gb-13-3-pd44087.html',
    urlnew: '/apple-macbook-pro-13-2017',
  },
  {
    urlold: '/asus-expertbook-p5440-pl493.html',
    urlnew: '/asus-expertbook-p5440',
  },
  {
    urlold: '/dell-alienware-m15',
    urlnew: '/dell-alienware-m15-r2',
  },
  {
    urlold: '/dell-alienware-m15-r2-pl501.html',
    urlnew: '/dell-alienware-m15-r2',
  },
  {
    urlold:
      '/dell-g7-2019/dell-g7-15-7590-i7-9750h-ram-16gb-ssd-256gb-hdd-1tb-fhd-ips-144hz-rtx-2060-pd43780.html',
    urlnew: '/dell-gaming-g7-7590',
  },
  {
    urlold: '/dell-g7-2019-pl448.html',
    urlnew: '/dell-gaming-g7-7590',
  },
  {
    urlold: '/san-pham/tag/dell-inspiron-7570-thinkpro-3353.html',
    urlnew: '/dell-inspiron-7570',
  },
  {
    urlold:
      '/dell-inspiron-7580/dell-inspiron-7580-i7-8565u-ram-16gb-ssd-512gb-fhd-webcam-pd43818.html',
    urlnew: '/dell-inspiron-7580',
  },
  {
    urlold:
      '/dell-inspiron-7590/dell-inspiron-7590-i5-9300h-ram-8gb-ssd-256gb-fhd-gtx1050-pd44132.html',
    urlnew: '/dell-inspiron-7590',
  },
  {
    urlold: '/dell-inspiron-7590-pl564.html',
    urlnew: '/dell-inspiron-7590',
  },
  {
    urlold: '/dell-latitude-e5470-pl495.html',
    urlnew: '/dell-latitude-e5470',
  },
  {
    urlold:
      '/dell-latitude-5480/dell-latitude-5480-i5-7300u-ram-8gb-ssd-256gb-hd-pd43940.html',
    urlnew: '/dell-latitude-5480',
  },
  {
    urlold: '/dell-latitude-5480-pl499.html',
    urlnew: '/dell-latitude-5480',
  },
  {
    urlold: '/dell-latitude-5500-pl528.html',
    urlnew: '/dell-latitude-5500',
  },
  {
    urlold: '/dell-latitude-e5570-pl494.html',
    urlnew: '/dell-latitude-e5570',
  },
  {
    urlold:
      '/danh-gia/dell-latitude-7390-chiec-laptop-13-inch-the-he-moi-cua-dell-nd497860.html',
    urlnew: '/dell-latitude-7390',
  },
  {
    urlold: '/san-pham/tag/precision-m4800-2034.html',
    urlnew: '/dell-precision-m4800',
  },
  {
    urlold: '/san-pham/tag/dell-precision-5510-738.html',
    urlnew: '/dell-precision-5510',
  },
  {
    urlold:
      '/danh-gia/dell-precision-5520-suc-manh-cua-may-tram-voi-than-hinh-cua-mot-ultrabook-cao-cap-nd497770.html',
    urlnew: '/dell-precision-5520',
  },
  {
    urlold:
      '/dell-precision-5540/dell-precision-5540-i7-9750h-ram-16gb-ssd-256gb-fhd-ips-t1000-pd43984.html',
    urlnew: '/dell-precision-5540',
  },
  {
    urlold: '/dell-precision-7510-pl334.html',
    urlnew: '/dell-precision-7510',
  },
  {
    urlold: '/san-pham/tag/dell-precision-7520-1035.html',
    urlnew: '/dell-precision-7520',
  },
  {
    urlold: '/dell-precision-7530-pl336.html',
    urlnew: '/thuong-hieu/dell',
  },
  {
    urlold: '/dell-precision-7540-pl512.html',
    urlnew: '/dell-precision-7540',
  },
  {
    urlold: '/dell-xps-13-9370-pl293.html',
    urlnew: '/dell-xps-13-9370',
  },
  {
    urlold: '/san-pham/tag/dell-xps-15-9570-thinkpro-3005.html',
    urlnew: '/dell-xps-15-9570',
  },
  {
    urlold:
      '/lenovo-ideacentre-730s-all-in-one/lenovo-ideacentre-730s-aio-i7-8550u-hdd-1tb-ram-8gb-23-8-fhd-ips-touch-pd43939.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold: '/lenovo-ideacentre-730s-all-in-one-pl498.html',
    urlnew: '/thuong-hieu/lenovo',
  },
  {
    urlold:
      '/dell-xps-15-9500/dell-xps-15-9500-i7-10750h-ram-8gb-ssd-256gb-fhd-gtx-1650-ti-pd44214.html',
    urlnew: '/dell-xps-15-9500',
  },
  {
    urlold:
      'dell-xps-17-9700/dell-xps-17-9700-i7-10750h-ram-16gb-ssd-512gb-fhd-gtx1650ti-pd44211.html',
    urlnew: '/dell-xps-17-9700',
  },
  {
    urlold: '/dell-xps-17-pl573.html',
    urlnew: '/dell-xps-17-9700',
  },
  {
    urlold:
      '/hp-elitebook-x360-1030-g3/hp-elitebook-x360-1030-g3-i5-8350u-ram-8gb-ssd-256gb-13-3-ips-fhd-touch-pd44141.html',
    urlnew: '/hp-elitebook-x360-1030-g3',
  },
  {
    urlold: '/hp-elitebook-840-g5-pl549.html',
    urlnew: '/hp-elitebook-840-g5',
  },
  {
    urlold:
      '/san-pham-moi/hp-envy-13-envy-15-va-envy-17-2019-se-so-huu-tam-man-amoled-tuy-chon-optane-va-co-ca-geforce-mx250-nd497932.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold: '/hp-envy-15-x360-pl509.html',
    urlnew: '/hp-envy-15-x360',
  },
  {
    urlold: '/san-pham/tag/hp-spectre-x360-13t-2018-3583.html',
    urlnew: '/hp-spectre-13-x360-2018',
  },
  {
    urlold:
      '/hp-spectre-x360-13-2019/hp-spectre-x360-13-2019-i5-8265u-ssd-256gb-ram-8gb-fhd-ips-touch-pd43799.html',
    urlnew: '/hp-pectre-13-x360-2019',
  },
  {
    urlold: '/hp-spectre-x360-13-2020-pl565.html',
    urlnew: '/hp-spectre-13-x360-2020',
  },
  {
    urlold:
      '/danh-gia/hp-zbook-studio-g3-may-tram-cho-nhung-nguoi-thich-nhe-nhang-thanh-lich-nd497712.html',
    urlnew: '/hp-zbook-studio-g3',
  },
  {
    urlold: '/tin-cong-nghe/hp-zbook-15v-g5-may-tram-gia-re-nd497847.html',
    urlnew: '/hp-zbook-15v-g5',
  },
  {
    urlold: '/hp-zbook-17-g5-pl570.html',
    urlnew: '/hp-zbook-17-g5',
  },
  {
    urlold:
      '/hp-zbook-17-g5/hp-zbook-17-g5-core-i7-8750h-ram-8gb-ssd-256gb-p1000-17-3-fhd-ips-pd44177.html',
    urlnew: '/hp-zbook-17-g5',
  },
  {
    urlold: '/lenovo-ideapad-l340-pl488.html',
    urlnew: '/lenovo-ideapad-L340',
  },
  {
    urlold: '/lenovo-ideapad-s540-pl555.html',
    urlnew: '/lenovo-ideapad-s540-14',
  },
  {
    urlold:
      '/lenovo-ideapad-s540/lenovo-ideapad-s540-14iwl-core-i5-8265u-ram-8gb-ssd-512gb-14-fhd-ips-touch-pd44169.html',
    urlnew: '/lenovo-ideapad-s540-14',
  },
  {
    urlold: '/lenovo-ideapad-s540-pl555.html',
    urlnew: '/lenovo-ideapad-s540-14',
  },
  {
    urlold: '/lenovo-legion-c730-pl534.html',
    urlnew: '/lenovo-legion-c730',
  },
  {
    urlold: '/lenovo-legion-y7000-pl407.html',
    urlnew: '/lenovo-legion-y7000',
  },
  {
    urlold: '/lenovo-thinkbook-14s-pl540.html',
    urlnew: '/lenovo-ideapad-s540-14',
  },
  {
    urlold: '/thinkpad-p1-mobile-workstation-pl406.html',
    urlnew: '/lenovo-thinkpad-p1-gen-1',
  },
  {
    urlold: '/thinkpad-p1-gen-2-pl481.html',
    urlnew: '/lenovo-thinkpad-p1-gen-2',
  },
  {
    urlold:
      '/thinkpad-p43s/thinkpad-p43s-i7-8565u-ram-16gb-ssd-512gb-fhd-p520-pd43854.html',
    urlnew: '/lenovo-thinkpad-p43s',
  },
  {
    urlold: '/san-pham/tag/thinkpad-p50-985.html',
    urlnew: '/lenovo-thinkpad-p50',
  },
  {
    urlold:
      'https://thinkpro.vn/danh-gia/lenovo-thinkpad-p52s-chiec-may-tram-cho-thoi-luong-pin-ca-ngay-nd497885.html',
    urlnew: '/lenovo-thinkpad-p52s',
  },
  {
    urlold: '/thinkpad-t470-pl295.html',
    urlnew: '/lenovo-thinkpad-t470',
  },
  {
    urlold: '/thinkpad-t470s-pl273.html',
    urlnew: '/lenovo-thinkpad-t470s',
  },
  {
    urlold:
      '/danh-gia/lenovo-thinkpad-t470s-su-lua-chon-tuyet-voi-danh-cho-ca-nhan-va-doanh-nghiep-nd497771.html',
    urlnew: '/lenovo-thinkpad-t470s',
  },
  {
    urlold: '/thinkpad-t480-pl318.html',
    urlnew: '/lenovo-thinkpad-t480',
  },
  {
    urlold: '/thinkpad-t480s-pl303.html',
    urlnew: '/lenovo-thinkpad-t480s',
  },
  {
    urlold: '/thinkpad-t490s-pl444.html',
    urlnew: '/lenovo-thinkpad-t490s',
  },
  {
    urlold: '/thinkpad-t495s-pl572.html',
    urlnew: '/lenovo-thinkpad-t495s',
  },
  {
    urlold: '/thinkpad-w540-pl542.html',
    urlnew: '/lenovo-thinkpad-w540',
  },
  {
    urlold: '/thinkpad-w541-pl543.html',
    urlnew: '/lenovo-thinkpad-w541',
  },
  {
    urlold: '/thinkpad-x1-carbon-gen-5-pl408.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-5',
  },
  {
    urlold: '/thinkpad-x1-extreme-pl405.html',
    urlnew: '/lenovo-thinkpad-x1-extreme-gen-1',
  },
  {
    urlold:
      '/danh-gia/thinkpad-x270-laptop-sieu-di-dong-pin-len-toi-21-gio-danh-cho-doanh-nghiep-nd497791.html',
    urlnew: '/lenovo-thinkpad-x270',
  },
  {
    urlold: '/thinkpad-x395-pl569.html',
    urlnew: '/lenovo-thinkpad-x395',
  },
  {
    urlold: '/thinkpad-x1-carbon-gen-5-pl408.html',
    urlnew: '/lenovo-thinkpad-x1-carbon-gen-5',
  },
  {
    urlold:
      '/razer-blade/razer-blade-2017-i7-7700hq-ram-16gb-ssd-512gb-fhd-gtx-1060-6gb-pd43452.html',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold: '/razer-blade-pro-pl290.html',
    urlnew: '/new-razer-blade-pro-17',
  },
  {
    urlold: '/razer-blade-stealth-12-5-pl343.html',
    urlnew: '/razer-blade-stealth-125',
  },
  {
    urlold:
      '/razer-blade-stealth-12-5/razer-blade-stealth-2017-i7-7500u-ram-16gb-ssd-512gb-12-5-4k-uhd-pd43473.html',
    urlnew: '/razer-blade-stealth-125',
  },
  {
    urlold: '/razer-blade-stealth-13-3-pl344.html',
    urlnew: '/razer-blade-stealth-133',
  },
  {
    urlold:
      '/razer-blade-stealth-13-3/razer-blade-stealth-13-3-2019-i7-8565u-ram-16gb-ssd-256gb-mx150-fhd-ips-pd43848.html',
    urlnew: '/razer-blade-stealth-133',
  },
  {
    urlold:
      '/tin-cong-nghe/bo-doi-gaming-mo-i-alienware-m15-va-m17-r3-co-gi-nd498612.html',
    urlnew: '/dell-alienware-m15-r3',
  },
  {
    urlold: '/razer-pl227',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold: '/razer-blade-15-pl437',
    urlnew: '/razer-blade-15-advanced',
  },
  {
    urlold:
      '/razer-pl227.html?budget=0&cpu=0&ram=0&hdd=0&display=0&company=0&use=0&vga=0&weight=0&status=0&sort=3',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold: '/razer-blade-pro-pl290.html',
    urlnew: '/new-razer-blade-pro-17',
  },
  {
    urlold: '/danh-gia/hp-spectre-x360-dep-va-cao-cap-nd497648.html',
    urlnew: '/hp-spectre-13-x360-2020',
  },
  {
    urlold:
      '/dell-inspiron-7580/dell-inspiron-7580-i7-8565u-ram-16gb-ssd-512gb-fhd-webcam-pd43818.html',
    urlnew: '/dell-inspiron-7580',
  },
  {
    urlold:
      '/dell-alienware-17-r5/dell-alienware-17-r5-i7-8750h-ram-16gb-ssd-256gb-hdd-1tb-fhd-ips-gtx-1070-pd43602.html',
    urlnew: '/dell-alienware-m17-r3',
  },
  {
    urlold:
      '/dan-cong-nghe/cach-do-cac-thong-so-cua-cpu-va-gpu-khi-choi-game-nd498025.html',
    urlnew: '/thuong-hieu/razer',
  },
  {
    urlold: '/tin-tuc/tin-tuc/cach-lua-chon-cpu-laptop-phu-hop-nhu-cau',
    urlnew: '/tin-tuc',
  },
  {
    urlold:
      '/danh-gia/lenovo-thinkpad-t470-may-tinh-xach-tay-kinh-doanh-dien-hinh-nd497837.html',
    urlnew: '/lenovo-thinkpad-t470',
  },
  {
    urlold:
      '/dell-precision-7510/dell-precision-7510-i7-6820hq-ram-8gb-ssd-256gb-fhd-ips-m2000m-pd43907.html',
    urlnew: '/dell-precision-7510',
  },
  {
    urlold:
      '/dell-xps-17-9700/dell-xps-17-9700-i7-10750h-ram-16gb-ssd-512gb-fhd-gtx1650ti-pd44211.html',
    urlnew: '/dell-xps-9700',
  },
  {
    urlold:
      '/dell-alienware-m15-r2/dell-alienware-m15-r2-i7-9750h-ram-16gb-ssd-512gb-fhd-ips-144hz-rtx-2060-pd43950.html',
    urlnew: '/dell-alienware-m15-r2',
  },
  {
    urlold:
      '/hp-envy-13/hp-envy-13-core-i5-8250u-ram-8gb-ssd-128gb-fhd-pd43679.html',
    urlnew: '/hp-envy-13-2019',
  },
  {
    urlold: '/san-pham/tag/hp-envy-13-2018-2674.html',
    urlnew: '/hp-envy-13-2019',
  },
  {
    urlold: '/hp-envy-13-pl471.html',
    urlnew: '/hp-envy-13-2019',
  },
  {
    urlold: '/tin-tuc/tag/hp-envy-13-2669.html',
    urlnew: '/hp-envy-13-2019',
  },
  {
    urlold:
      '/hp-envy-13-2018/hp-envy-13t-core-i7-8550u-ram-8gb-ssd-256gb-fhd-ips-touch-pd43634.html',
    urlnew: '/hp-envy-13-2019',
  },
  {
    urlold: '/san-pham/tag/hp-envy-13t-thinkpro-3673.html',
    urlnew: '/thuong-hieu/hp',
  },
  {
    urlold: '/tin-tuc/tag/acer-nitro-5-spin-2223.html',
    urlnew: '/acer-nitro-5',
  },
  {
    urlold:
      '/acer-nitro-5/acer-nitro-5-i5-9300h-ram-8gb-ssd-256gb-gtx-1650-fhd-ips-pd44218.html',
    urlnew: '/acer-nitro-5',
  },
  {
    urlold: '/san-pham/tag/ban-acer-nitro-5-tai-ha-noi-3112.html',
    urlnew: '/acer-nitro-5',
  },
  {
    urlold: '/san-pham/tag/laptop-gaming-671.html',
    urlnew: '/acer-nitro-5',
  },
  {
    urlold:
      '/danh-gia/hp-zbook-15-g4-may-tram-di-dong-danh-cho-chuyen-gia-nd497867.html',
    urlnew: '/hp-zbook-15-g5',
  },
]
