import redirectSSL from 'redirect-ssl'
import Helper from './assets/config'

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: 'vi',
    },
    title: 'Thinkpro - Hệ thống Máy tính và phụ kiện',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Hệ thống trải nghiệm và bán lẻ Laptop, PC giá tốt, tư vấn chính xác, bảo hành tận tâm',
      },
      {
        hid: 'lang',
        name: 'lang',
        content: 'vi',
      },
      {
        hid: 'author',
        name: 'author',
        content: 'Thinkpro',
      },
      {
        hid: 'theme-color',
        name: 'theme-color',
        content: '#06C1D4',
      },
      {
        hid: 'og:type',
        name: 'og:type',
        content: 'website',
      },
      {
        hid: 'og:site_name',
        name: 'og:site_name',
        content: 'Thinkpro',
      },
      {
        hid: 'og:url',
        name: 'og:url',
        content: 'https://thinkpro.vn/',
      },
      {
        hid: 'og:title',
        name: 'og:title',
        content: 'Thinkpro - Hệ thống Máy tính và phụ kiện',
      },
      {
        hid: 'og:description',
        name: 'og:description',
        content:
          'Hệ thống trải nghiệm và bán lẻ Laptop, PC giá tốt, tư vấn chính xác, bảo hành tận tâm',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      { hid: 'caresoft', src: '/caresoft.js', async: false, defer: true },
    ],
  },

  router: {
    // ran before every route on both client and server
    middleware: ['redirect'],
  },

  serverMiddleware: [
    redirectSSL.create({
      enabled: Helper.production,
    }),
  ],
  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/loading.vue',
  /*
   ** Global CSS
   */

  css: ['~/assets/css/style.scss'],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/filter',
    { src: '~/plugins/localStorage.js', ssr: false },
    { src: '~/plugins/vue-awesome-swiper.js', ssr: true },
    { src: '~plugins/ga.js', ssr: false },
    { src: '~/plugins/vue-lazysizes.client.js', ssr: false },
    { src: '~/plugins/vue-observe-visibility.js', ssr: false },
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    // '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    // '@nuxtjs/stylelint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/device',
    '@nuxtjs/axios',
    '@nuxtjs/sitemap',
    '@nuxtjs/auth',
    '@nuxtjs/svg-sprite',
  ],
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/login', method: 'post', propertyName: 'token' },
          user: { url: '/user', method: 'get', propertyName: '' },
          logout: { url: '/logout', method: 'get' },
        },
      },
    },
    redirect: false,
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: Helper.baseUrl,
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, { isDev, isClient, loaders: { vue } }) {
      if (isClient) {
        vue.transformAssetUrls.img = ['data-src', 'src']
        vue.transformAssetUrls.source = ['data-srcset', 'srcset']
      }
    },
  },
  svgSprite: {
    input: '~/assets/icons/',
    spriteClassPrefix: 'icon-',
  },
  sitemap: {
    hostname: 'https://thinkpro.vn',
    gzip: true,
  },
  render: {
    static: {
      maxAge: 1000 * 60 * 60 * 24 * 7,
    },
  },
}
