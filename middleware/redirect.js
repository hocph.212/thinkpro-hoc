import JsonUrls from '~/plugins/url-json'
export default function (ctx) {
  const found = JsonUrls.find(
    (element) => element.urlold === ctx.route.fullPath
  )
  if (found) {
    ctx.redirect(301, ctx.req.headers.host + found.urlnew)
  }
}
